/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego_futbol_ia;

import java.util.TimerTask;


/**
 *
 * @author DANIEL
 */
public class Adversario extends TimerTask implements Constantes{
    
    public Laberinto laberinto;
    public Celda celdaAdversario;
    
    int gol_adversario=0;
    
    public BusquedaProfundidad buscaPelota;
    public BusquedaAnchura llevaPelota;
    
    public boolean traeAdversario = false;
    
    int valor, valor2;
    
    public boolean gol_abajo=false;
    public boolean gol_arriba=false;
    
    
    public boolean parar;
    
    public Adversario(Laberinto laberinto, int tiempoDormido){
        this.laberinto=laberinto;
        celdaAdversario=new Celda(27, 8,'A');
    
        laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
        
        buscaPelota= new BusquedaProfundidad(laberinto, this, laberinto.lienzoPadre.jugador);
        llevaPelota= new BusquedaAnchura(laberinto,laberinto.lienzoPadre.jugador, this);

    }
    
    public void acomodarPelota(int x, int y){
        
        if(laberinto.celdas[x-1][y-1].tipo != 'O'){
            laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
            laberinto.celdaPelota.y=laberinto.celdaPelota.y-1;
            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo = 'L';
            laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y].tipo = 'V';
        }else{
            laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
            laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo = 'L';
            laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y].tipo = 'V';
        }
  

    }

    public void moverCeldaArriba(){
        //agregue para que no se coma al adversario lo final
        if (celdaAdversario.y > 1  ) {
           
            if(laberinto.celdas[celdaAdversario.x][celdaAdversario.y-1].tipo != 'J'){
                if(laberinto.celdas[celdaAdversario.x][celdaAdversario.y-1].tipo != 'O'){
                    if(laberinto.celdas[celdaAdversario.x][celdaAdversario.y-1].tipo == 'L'){
                        if(celdaAdversario.y-3 >= 0 ){
                            if(laberinto.celdas[celdaAdversario.x][celdaAdversario.y-2].tipo != 'J'){
                                if(laberinto.celdas[celdaAdversario.x][celdaAdversario.y-2].tipo != 'O'){
                                    
                                    if(laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y-1].tipo == 'J' || laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y-1].tipo == 'O'){
                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y-1;
                                    }else{
                                        laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                    }
                                    //PATEA LA PELOTA
                                    laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                    celdaAdversario.y=celdaAdversario.y-1;
                                    laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A'; 
                                    laberinto.celdas[celdaAdversario.x][celdaAdversario.y+1].tipo='V';
                                    traeAdversario = true;

                                }else{
                                    //ESQUIVA UN OBSTACULO
                                    switch(laberinto.lienzoPadre.levelA){
                                    case 1: //FACIL
                                        celdaAdversario.y=celdaAdversario.y-1;
                                        laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A'; 
                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                        break;
                                    case 2://DIFICIL
                                        if(laberinto.celdas[celdaAdversario.x+1][celdaAdversario.y-1].tipo != 'O' || laberinto.celdas[celdaAdversario.x+1][celdaAdversario.y-1].tipo != 'J'){
                                            moverCeldaArribaDerecha();
                                        }
                                        
                                        break;
                                    }
                                    
                                 }
                            }else{
                                //SI SE ENCUENTRA CON UN JUGADOR
                                switch(laberinto.lienzoPadre.levelA){
                                    case 1: //FACIL
                                        if(laberinto.celdas[celdaAdversario.x+1][celdaAdversario.y].tipo != 'O'){
                                            moverCeldaDerecha();
                                            if(laberinto.celdas[celdaAdversario.x][celdaAdversario.y-1].tipo != 'O'){
                                                moverCeldaArriba();
                                            }else{
                                                moverCeldaIzquierda();
                                                moverCeldaArriba();
                                            }
                                            
                                        }else{
                                            if(laberinto.celdas[celdaAdversario.x+1][celdaAdversario.y-1].tipo != 'O'){
                                                moverCeldaArriba();
                                                moverCeldaDerecha();
                                            }else{
                                                if(laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y+1].tipo != 'O'){
                                                    moverCeldaIzquierda();
                                                    moverCeldaArriba();
                                                }
                                            }
                                            
                                        }
                                        laberinto.lienzoPadre.jugador.traeJugador=true;
                                        break;
                                    case 2://DIFICIL
                                        if(laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y].tipo != 'O'){
                                            laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                            laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                        }else{
                                            if(laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y+1].tipo != 'O'){
                                                laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                            }
                                        }
                                        this.traeAdversario=true;
                                        break;
                                }
                            }
                        }else{
                            laberinto.lateral();
                            /*
                            //LLEGA AL FINAL DE LA CANCHA
                             switch(laberinto.lienzoPadre.levelA){
                                 case 1://facil
                                     //TIRA LA PELOTA ABAJO DEL JUGADOR
                                    celdaAdversario.y=celdaAdversario.y-1;
                                    laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                    laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                    laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                                     break;
                                 case 2: //ficil
                                     //LA MUEVE A LA IZQUIERDA DEL JUGADOR
                                     if(laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y+1].tipo != 'J' || laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y+1].tipo != 'O'){
                                         laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                         laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                     }else{
                                        celdaAdversario.y=celdaAdversario.y-1;
                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                        laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                                     }
                                     break;
                             }
                            
                            */ 
                        }
                        
               }else{
                    //SE MUEVE PARA ARRIBA
                    laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='V';
                    celdaAdversario.y=celdaAdversario.y-1;
                    laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                    //laberinto.celdas[celdaAdversario.x][celdaAdversario.y].indexSprite=laberinto.arriba;
               }
                }
            }
        }
    }
    
        public void moverCeldaAbajo(){
        if (celdaAdversario.y < alturaMundoVirtual -2 ) {
            
            if(laberinto.celdas[celdaAdversario.x][celdaAdversario.y+1].tipo != 'J'){
                if(laberinto.celdas[celdaAdversario.x][celdaAdversario.y+1].tipo != 'O'){
                    if(laberinto.celdas[celdaAdversario.x][celdaAdversario.y+1].tipo == 'L'){
                        if(celdaAdversario.y+3 < alturaMundoVirtual){
                            if(laberinto.celdas[celdaAdversario.x][celdaAdversario.y+2].tipo != 'J'){
                                if(laberinto.celdas[celdaAdversario.x][celdaAdversario.y+2].tipo != 'O'){
                                    //PATEA LA PELOTA
                                    if(laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y+1].tipo == 'J' || laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y+1].tipo == 'O'){
                                            laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                    }else{
                                            laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                    }

                                    laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                    celdaAdversario.y=celdaAdversario.y+1;
                                    laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                                    laberinto.celdas[celdaAdversario.x][celdaAdversario.y-1].tipo='V';
                                    traeAdversario = true;
                         
                                    
                                }else{
                                    //ENCUENTRA UN OBSTACULO
                                    switch(laberinto.lienzoPadre.levelA){
                                    case 1: //FACIL
                                        celdaAdversario.y=celdaAdversario.y+1;
                                        laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A'; 
                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y-1;
                                        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                        break;
                                    case 2://DIFICIL
                                        moverCeldaAbajoDerecha();
                                        break;
                                    }
                                }
                            }else{
                                //SI SE ENCUENTRA CON UN JUGADOR
                                switch(laberinto.lienzoPadre.levelA){
                                    case 1: //FACIL
                                        moverCeldaDerecha();
                                        moverCeldaAbajo();
                                        laberinto.lienzoPadre.jugador.traeJugador=true;
                                        break;
                                    case 2://DIFICIL
                                        laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y-1;
                                        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                        this.traeAdversario=true;
                                        break;
                                }
                            }

                        }else{
                            laberinto.lateral();
                            /*
                            //LLEGA AL FINAL DE LA CANCHA
                            switch(laberinto.lienzoPadre.levelA){
                                case 1:
                                    celdaAdversario.y=celdaAdversario.y+1;
                                    laberinto.celdaPelota.y=laberinto.celdaPelota.y-1;
                                    laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                    laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                                    break;
                                case 2:
                                    if(laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y-1].tipo == 'J' || laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y-1].tipo == 'O'){
                                         laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                         laberinto.celdaPelota.y=laberinto.celdaPelota.y-1;
                                        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                     }else{
                                         laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                         laberinto.celdaPelota.y=laberinto.celdaPelota.y-2;
                                         laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                     }
                                    break;
                            }
                             
                             */
                        }
                        
                    }else{
                        laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='V';
                        celdaAdversario.y=celdaAdversario.y+1;
                        laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                       // laberinto.celdas[celdaAdversario.x][celdaAdversario.y].indexSprite=laberinto.abajo;
   

                    }
                }
            }
        }
    }
    
    public void moverCeldaIzquierda(){
        if (celdaAdversario.x > 0 ) {
            
            if(laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y].tipo!='J'){
                if(laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y].tipo!='O'){
                    if(laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y].tipo == 'L'){
                        //ESTA AL FINAL DE LA CANCHA
                        if(celdaAdversario.x-2 >= 0){
                            if(laberinto.lienzoPadre.levelA == 1){
                                //FACIL
                                valor=2;
                                valor2=2;
                            }else{
                                //DIFICIL
                                valor=Math.abs((int) laberinto.lienzoPadre.adversario.buscaPelota.distancia(celdaAdversario.x, celdaAdversario.y, laberinto.lienzoPadre.jugador.celdaJugador.x, laberinto.lienzoPadre.jugador.celdaJugador.y));  
                                valor2 = laberinto.calculaObstaculosA(celdaAdversario.x, celdaAdversario.y);
                            }
                            //SI NO HAY UN JUGADOR MAS ADELANTE
                            if(laberinto.celdas[Math.abs(celdaAdversario.x-valor)][celdaAdversario.y].tipo!='J'){
                                //SI NO HAY UN OBSTACULO MAS ADELANTE
                                if(laberinto.celdas[Math.abs(celdaAdversario.x-valor2)][celdaAdversario.y].tipo!='O'){
                                    //SI LA PELOTA ENTRA EN EL ARCO
                                    if((laberinto.celdas[celdaAdversario.x][celdaAdversario.y] == laberinto.celdas[1][5]) || 
                                                 (laberinto.celdas[celdaAdversario.x][celdaAdversario.y] == laberinto.celdas[1][6]) ||
                                                 (laberinto.celdas[celdaAdversario.x][celdaAdversario.y] == laberinto.celdas[1][7]) ||
                                                 (laberinto.celdas[celdaAdversario.x][celdaAdversario.y] == laberinto.celdas[1][8]) ||
                                                 (laberinto.celdas[celdaAdversario.x][celdaAdversario.y] == laberinto.celdas[1][9]) ||
                                                 (laberinto.celdas[celdaAdversario.x][celdaAdversario.y] == laberinto.celdas[1][10] ||
                                                 (laberinto.celdas[celdaAdversario.x][celdaAdversario.y] == laberinto.celdas[1][11])) ){

                                            System.out.println("Gol del Adversario");
                                            laberinto.lienzoPadre.adversario.gol_adversario=laberinto.lienzoPadre.adversario.gol_adversario +1;                      
                                            //laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y].tipo='V';
                                            //laberinto.celdaPelota.x=laberinto.celdaPelota.x;
                                            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                            laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y].tipo='V';
                                            laberinto.gol();
                                        }else{
                                            
                                            if(gol_abajo== true){
                                                //SE MUEVE LA PELOTA PARA ABAJO   
                                                if(laberinto.celdas[celdaAdversario.x-2][celdaAdversario.y+1].tipo != 'J'){
                                                    if(laberinto.celdas[celdaAdversario.x-2][celdaAdversario.y+1].tipo != 'O'){
                                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                                        laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y].tipo='V';
                                                        traeAdversario = true;
                                                        gol_abajo=false;  
                                                    }
                                                }
                                            }
                                            
                                            if(gol_arriba== true){
                                                //SE MUEVE LA PELOTA PARA ARRIBA  
                                                if(laberinto.celdas[celdaAdversario.x-2][celdaAdversario.y-1].tipo != 'J'){
                                                    if(laberinto.celdas[celdaAdversario.x-2][celdaAdversario.y-1].tipo != 'O'){
                                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y-1;
                                                        laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y].tipo='V';
                                                        traeAdversario = true;
                                                        gol_arriba=false;
                                                    }
                                                }
                                                
                                            }
                                            
                                           
                                            //SE MUEVE LA PELOTA   
                                            laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                            laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y].tipo='V';
                                            traeAdversario = true;

                                        }
                                }else{
                                    //ENCUENTRA UN OBSTACULO
                                    this.acomodarPelota(celdaAdversario.x, celdaAdversario.y);

                                }
                                            
                            }else{
                                //ENCUENTRA UN JUGADOR
                                this.acomodarPelota(celdaAdversario.x, celdaAdversario.y);
                                
                                if(laberinto.lienzoPadre.levelA == 1){
                                    laberinto.lienzoPadre.jugador.traeJugador=true;
                                }else{
                                    this.traeAdversario=true;
                                }
                            } 
                        }else{
                            if((laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y] == laberinto.celdas[0][5]) || 
                                                 (laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y] == laberinto.celdas[0][6]) ||
                                                 (laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y] == laberinto.celdas[0][7]) ||
                                                 (laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y] == laberinto.celdas[0][8]) ||
                                                 (laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y] == laberinto.celdas[0][9]) ||
                                                 (laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y] == laberinto.celdas[0][10] ||
                                                 (laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y] == laberinto.celdas[0][11])) ){
                                
                                System.out.println("Gol del Adversario");
                                laberinto.lienzoPadre.adversario.gol_adversario=laberinto.lienzoPadre.adversario.gol_adversario +1;                      
                                //laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y].tipo='V';
                                //laberinto.celdaPelota.x=laberinto.celdaPelota.x;
                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y].tipo='V';
                                laberinto.gol();
                            }else{
                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='V';
                                laberinto.celdaPelota.x=8;
                                laberinto.celdaPelota.y=8;
                                laberinto.saque();
                            }
                            
                            /*if(gol_abajo== true){
                                laberinto.celdaPelota.x=laberinto.celdaPelota.x+2;
                                laberinto.celdaPelota.y=laberinto.celdaPelota.y+2;
                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y].tipo='V'; 
                            }else{
                                
                            }
                            laberinto.celdaPelota.x=laberinto.celdaPelota.x+2;
                            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                            laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y].tipo='V'; */
                            
                        }
                    }else{
                        laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='V';
                        celdaAdversario.x=celdaAdversario.x-1;
                        laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                        //laberinto.celdas[celdaAdversario.x][celdaAdversario.y].indexSprite=laberinto.izquierda;

                    }
                }
            }            
        }
    }
    
    public void moverCeldaDerecha(){
        if ((celdaAdversario.x < anchuraMundoVirtual -1)) {
            
            if(laberinto.celdas[celdaAdversario.x+1][celdaAdversario.y].tipo!='J'){
                if(laberinto.celdas[celdaAdversario.x+1][celdaAdversario.y].tipo!='O'){
                    if(laberinto.celdas[celdaAdversario.x+1][celdaAdversario.y].tipo == 'L'){
                        if(celdaAdversario.x+1 < anchuraMundoVirtual){
                            if(laberinto.celdas[celdaAdversario.x+2][celdaAdversario.y].tipo!='J' ){
                                if(laberinto.celdas[celdaAdversario.x+2][celdaAdversario.y].tipo!='O'){
                                    
                                    switch(laberinto.lienzoPadre.levelA){
                                        //MUEVE LA PELOTA A LA IZQUIERDA 
                                        case 1:
                                            if(laberinto.celdas[celdaAdversario.x+1][celdaAdversario.y-1].tipo == 'J'){
                                                moverCeldaArriba();
                                                moverCeldaDerecha();

                                                traeAdversario = true;
                                            }else{
                                                moverCeldaAbajo();
                                                moverCeldaDerecha();
                                                traeAdversario = true;
                                            }
                                            break;
                                        case 2:
                                            laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                            celdaAdversario.x=celdaAdversario.x+1;
                                            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                            laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                                            traeAdversario = true;
                                            break;
                                    }
                                }else{
                              
                                    //MUEVE LA PELOTA A LA IZQUIERDA 

                                    laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                    celdaAdversario.x=celdaAdversario.x+1;
                                    laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                    laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                                    traeAdversario = true;
                                  
                                }
                            }else{
                                
                                celdaAdversario.x=celdaAdversario.x+1;
                                laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                                laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                
                                if(laberinto.lienzoPadre.levelA == 1){
                                    laberinto.lienzoPadre.jugador.traeJugador=true;
                                }else{
                                    this.traeAdversario=true;
                                }

                            }     
                        }else{
                             laberinto.celdas[celdaAdversario.x+1][celdaAdversario.y].tipo='V'; 
                             laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                             laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                        }
                        
                    }else{
                            laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='V';
                            celdaAdversario.x=celdaAdversario.x+1;
                            laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                            //laberinto.celdas[celdaAdversario.x][celdaAdversario.y].indexSprite=laberinto.derecha;

                            }
                    }
                }
            }
        }
    
     public void moverCeldaArribaIzquierda(){
          if(celdaAdversario.x > 0 && celdaAdversario.y > 1){
            if(laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y-1].tipo!='J'){
                if(laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y-1].tipo!='O'){
                    if(laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y-1].tipo == 'L'){
                        if(laberinto.celdaPelota.y-2 >= 0 ){
                            if(laberinto.celdaPelota.x > 0){
                                if(laberinto.celdas[celdaAdversario.x-2][celdaAdversario.y-2].tipo != 'J' ){
                                    if(laberinto.celdas[celdaAdversario.x-2][celdaAdversario.y-2].tipo != 'O'){
                                    
                                        //PARA NO BORRAR JUGADOR+
                                        if(laberinto.celdas[celdaAdversario.x-2][celdaAdversario.y-1].tipo != 'J'){
                                            if(laberinto.celdas[celdaAdversario.x-2][celdaAdversario.y-1].tipo != 'O'){
                                                //MUEVE LA PELOTA
                                                laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                                celdaAdversario.x=celdaAdversario.x-1;
                                                celdaAdversario.y=celdaAdversario.y-1;
                                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                                laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                                                laberinto.celdas[celdaAdversario.x+1][celdaAdversario.y+1].tipo='V'; 
                                            }else{
                                                //MUEVE LA PELOTA
                                                laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                                laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                                celdaAdversario.x=celdaAdversario.x-1;
                                                celdaAdversario.y=celdaAdversario.y-1;
                                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                                laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                                                laberinto.celdas[celdaAdversario.x+1][celdaAdversario.y+1].tipo='V'; 
                                            }
                                        }else{
                                            //MUEVE LA PELOTA
                                            laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                            laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                            celdaAdversario.x=celdaAdversario.x-1;
                                            celdaAdversario.y=celdaAdversario.y-1;
                                            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                            laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                                            laberinto.celdas[celdaAdversario.x+1][celdaAdversario.y+1].tipo='V'; 
                                        }
                                    
                                        
                                    
                                    } /*else{
                                        //CON PELOTA SE PILLA CON OBSTACULO
                                        laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y-1].tipo='V'; 
                                        laberinto.celdaPelota.x=laberinto.celdaPelota.x+2;
                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y+2;
                                        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                    }*/
                                }else{
                                    //MUEVE LA PELOTA
                                    laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                    celdaAdversario.x=celdaAdversario.x-1;
                                    celdaAdversario.y=celdaAdversario.y-1;
                                    laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                    laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                                    laberinto.celdas[celdaAdversario.x+1][celdaAdversario.y+1].tipo='V'; 
                                    
                                    if(laberinto.lienzoPadre.levelA == 1){
                                        laberinto.lienzoPadre.jugador.traeJugador=true;
                                    }else{
                                        this.traeAdversario=true;
                                    }
                                }
                            }else{
                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='V';
                                laberinto.celdaPelota.x=8;
                                laberinto.celdaPelota.y=8;
                                laberinto.saque();
                                 
                            }
                        }else{
                            laberinto.lateral();
                            /*
                            //SI CHOCA AL FINAL DE LA CANCHA
                            if(laberinto.celdas[celdaAdversario.x-2][celdaAdversario.y].tipo != 'J'){
                                if(laberinto.celdas[celdaAdversario.x-2][celdaAdversario.y].tipo != 'O'){
                                    laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y-1].tipo='V'; 
                                    laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                    laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                    laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                }else{
                                    laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y-1].tipo='V'; 
                                    laberinto.celdaPelota.x=laberinto.celdaPelota.x+2;
                                    laberinto.celdaPelota.y=laberinto.celdaPelota.y+2;
                                    laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                }
                            }else{
                                laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y-1].tipo='V'; 
                                laberinto.celdaPelota.x=laberinto.celdaPelota.x+2;
                                laberinto.celdaPelota.y=laberinto.celdaPelota.y+2;
                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                            }*/
                        }
                    }else{
                        laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='V';
                        celdaAdversario.x=celdaAdversario.x-1;
                        celdaAdversario.y=celdaAdversario.y-1;
                        laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                        
                    }
                }
            }
        }
     }
     
     public void moverCeldaAbajoIzquierda(){
          if(celdaAdversario.x > 0 && celdaAdversario.y < alturaMundoVirtual -2){
            if(laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y+1].tipo!='J'){
                if(laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y+1].tipo!='O'){
                    if(laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y+1].tipo == 'L'){
                        if( laberinto.celdaPelota.y+3 < alturaMundoVirtual ){
                            if(laberinto.celdaPelota.x-2 >= 0 ){
                                if(laberinto.celdas[celdaAdversario.x-2][celdaAdversario.y+2].tipo != 'J'){
                                    if(laberinto.celdas[celdaAdversario.x-2][celdaAdversario.y+2].tipo != 'O'){
                                        //PARA QUE NO BORRE ADVERSARIO
                                        if(laberinto.celdas[celdaAdversario.x-2][celdaAdversario.y+1].tipo != 'J'){
                                            if(laberinto.celdas[celdaAdversario.x-2][celdaAdversario.y+1].tipo != 'O'){
                                                //MUEVE LA PELOTA
                                                laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                                celdaAdversario.x=celdaAdversario.x-1;
                                                celdaAdversario.y=celdaAdversario.y+1;
                                                laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                                laberinto.celdas[celdaAdversario.x+1][celdaAdversario.y-1].tipo='V'; 
                                            }else{
                                                laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                                laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                                celdaAdversario.x=celdaAdversario.x-1;
                                                celdaAdversario.y=celdaAdversario.y+1;
                                                laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                                laberinto.celdas[celdaAdversario.x+1][celdaAdversario.y-1].tipo='V'; 
                                            }
                                        }else{
                                            laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                            laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                            celdaAdversario.x=celdaAdversario.x-1;
                                            celdaAdversario.y=celdaAdversario.y+1;
                                            laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                                            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                            laberinto.celdas[celdaAdversario.x+1][celdaAdversario.y-1].tipo='V'; 
                                            
                                            if(laberinto.lienzoPadre.levelA == 1){
                                                laberinto.lienzoPadre.jugador.traeJugador=true;
                                            }else{
                                                this.traeAdversario=true;
                                            }
                                        }


                                    }else{
                                        //CON PELOTA SE PILLA CON OBSTACULO
                                        laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y+1].tipo='V'; 
                                        laberinto.celdaPelota.x=laberinto.celdaPelota.x+2;
                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y-2;
                                        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                    }
                                }else{
                                    //MUEVE LA PELOTA
                                    laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                    celdaAdversario.x=celdaAdversario.x-1;
                                    celdaAdversario.y=celdaAdversario.y+1;
                                    laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                    laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                                    laberinto.celdas[celdaAdversario.x+1][celdaAdversario.y-1].tipo='V'; 
                                    
                                    if(laberinto.lienzoPadre.levelA == 1){
                                        laberinto.lienzoPadre.jugador.traeJugador=true;
                                    }else{
                                        this.traeAdversario=true;
                                    }
                                }
                            }else{
                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='V';
                                laberinto.celdaPelota.x=8;
                                laberinto.celdaPelota.y=8;
                                laberinto.saque(); 
                            }
                        }else{
                            laberinto.lateral();
                            /*
                            //SI CHOCA AL FINAL DE LA CANCHA
                                if(laberinto.celdas[celdaAdversario.x-2][celdaAdversario.y].tipo != 'J'){
                                    if(laberinto.celdas[celdaAdversario.x-2][celdaAdversario.y].tipo != 'O'){
                                        laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y+1].tipo='V'; 
                                        laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y-1;
                                        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                    }else{
                                        laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y+1].tipo='V'; 
                                        laberinto.celdaPelota.x=laberinto.celdaPelota.x+2;
                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y-2;
                                        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                    }
                                }else{
                                    laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y+1].tipo='V'; 
                                    laberinto.celdaPelota.x=laberinto.celdaPelota.x+2;
                                    laberinto.celdaPelota.y=laberinto.celdaPelota.y-2;
                                    laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                    
                                    if(laberinto.lienzoPadre.levelA == 1){
                                        laberinto.lienzoPadre.jugador.traeJugador=true;
                                    }else{
                                        this.traeAdversario=true;
                                    }
                                }*/
                        }
                    }else{
                        laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='V';
                        celdaAdversario.x=celdaAdversario.x-1;
                        celdaAdversario.y=celdaAdversario.y+1;
                        laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                        
                    }
                }
            }
        }
     }
     
     public void moverCeldaArribaDerecha(){
        if(celdaAdversario.x < anchuraMundoVirtual -1 && celdaAdversario.y > 1 ){
            
            if(laberinto.celdas[celdaAdversario.x+1][celdaAdversario.y-1].tipo!='J'){
                if(laberinto.celdas[celdaAdversario.x+1][celdaAdversario.y-1].tipo!='O'){
                    if(laberinto.celdas[celdaAdversario.x+1][celdaAdversario.y-1].tipo == 'L'){
                        if(celdaAdversario.x+1 < anchuraMundoVirtual-1 && celdaAdversario.y-3 >= 0 ){
                            if(laberinto.celdas[celdaAdversario.x+2][celdaAdversario.y-2].tipo != 'J'){
                                if(laberinto.celdas[celdaAdversario.x+2][celdaAdversario.y-2].tipo != 'O'){
                                   
                                    if(laberinto.celdas[celdaAdversario.x][celdaAdversario.y-1].tipo != 'J'){
                                        if(laberinto.celdas[celdaAdversario.x][celdaAdversario.y-1].tipo != 'O'){
                                            //MUEVE LA PELOTA
                                            laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                            celdaAdversario.x=celdaAdversario.x+1;
                                            celdaAdversario.y=celdaAdversario.y-1;
                                            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                            laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A'; 
                                            laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y+1].tipo='V'; 
                                            traeAdversario=true; 
                                        }else{
                                            //MUEVE LA PELOTA
                                            laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                            laberinto.celdaPelota.y=laberinto.celdaPelota.y-1;
                                            celdaAdversario.x=celdaAdversario.x+1;
                                            celdaAdversario.y=celdaAdversario.y-1;
                                            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                            laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                                            laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y+1].tipo='V'; 
                                        }
                                    }else{
                                        //MUEVE LA PELOTA
                                        laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y-1;
                                        celdaAdversario.x=celdaAdversario.x+1;
                                        celdaAdversario.y=celdaAdversario.y-1;
                                        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                        laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                                        laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y+1].tipo='V'; 
                                    }
                                    
                                }else{
                                    //SE ENCUENTRA UN OBSTACULO
                                    laberinto.celdas[celdaAdversario.x+1][celdaAdversario.y-1].tipo='V';
                                    laberinto.celdaPelota.x=laberinto.celdaPelota.x-2;
                                    laberinto.celdaPelota.y=laberinto.celdaPelota.y+2;
                                    laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                }
                            }else{
                             
                                  //MUEVE LA PELOTA
                                    laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                    celdaAdversario.x=celdaAdversario.x+1;
                                    celdaAdversario.y=celdaAdversario.y-1;
                                    laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                    laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                                    laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y+1].tipo='V';   
                               
                                if(laberinto.lienzoPadre.levelA == 1){
                                    laberinto.lienzoPadre.jugador.traeJugador=true;
                                }else{
                                    this.traeAdversario=true;
                                }
                            }
                        }else{
                            laberinto.lateral();
                            /*
                            //FINAL DEL MUNDO
                             laberinto.celdas[celdaAdversario.x+1][celdaAdversario.y-1].tipo='V'; 
                             laberinto.celdaPelota.x=laberinto.celdaPelota.x-2;
                             laberinto.celdaPelota.y=laberinto.celdaPelota.y+2;
                             laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';*/
                        }
                    }else{
                        laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='V';
                        celdaAdversario.x=celdaAdversario.x+1;
                        celdaAdversario.y=celdaAdversario.y-1;
                        laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                    }
                }
            }
        }
    }
     
     public void  moverCeldaAbajoDerecha(){
        if(celdaAdversario.x < anchuraMundoVirtual -1 && celdaAdversario.y < alturaMundoVirtual -2 ){
            
            if(laberinto.celdas[celdaAdversario.x+1][celdaAdversario.y+1].tipo!='J'){
                if(laberinto.celdas[celdaAdversario.x+1][celdaAdversario.y+1].tipo!='O'){
                    if(laberinto.celdas[celdaAdversario.x+1][celdaAdversario.y+1].tipo == 'L'){
                        if(celdaAdversario.x+1 < anchuraMundoVirtual-1 && celdaAdversario.y+3 < alturaMundoVirtual){
                            if(laberinto.celdas[celdaAdversario.x+2][celdaAdversario.y+2].tipo != 'J'){
                                if(laberinto.celdas[celdaAdversario.x+2][celdaAdversario.y+2].tipo != 'O'){
                                    
                                    if(laberinto.celdas[celdaAdversario.x][celdaAdversario.y+1].tipo != 'J'){
                                        if(laberinto.celdas[celdaAdversario.x][celdaAdversario.y+1].tipo != 'O'){
                                            //MUEVE LA PELOTA
                                            laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                            celdaAdversario.x=celdaAdversario.x+1;
                                            celdaAdversario.y=celdaAdversario.y+1;
                                            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                            laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                                            laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y-1].tipo='V'; 
                                            traeAdversario=true;
                                        }else{
                                            //MUEVE LA PELOTA
                                            laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                            laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                            celdaAdversario.x=celdaAdversario.x+1;
                                            celdaAdversario.y=celdaAdversario.y+1;
                                            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                            laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                                            laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y-1].tipo='V'; 
                                            traeAdversario=true;
                                        }
                                    }else{
                                        //MUEVE LA PELOTA
                                        laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                        celdaAdversario.x=celdaAdversario.x+1;
                                        celdaAdversario.y=celdaAdversario.y+1;
                                        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                        laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                                        laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y-1].tipo='V'; 
                                        
                                        if(laberinto.lienzoPadre.levelA == 1){
                                            laberinto.lienzoPadre.jugador.traeJugador=true;
                                        }else{
                                            this.traeAdversario=true;
                                        }
                                    }
                                    
                                    
                                }else{
                                    //SE ENCUENTRA CON UN OBSTACULO
                                    laberinto.celdas[celdaAdversario.x+1][celdaAdversario.y+1].tipo='V'; 
                                    laberinto.celdaPelota.x=laberinto.celdaPelota.x-2;
                                    laberinto.celdaPelota.y=laberinto.celdaPelota.y-2;
                                    laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                }
                            }else{

                                    //MUEVE LA PELOTA
                                    laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                    celdaAdversario.x=celdaAdversario.x+1;
                                    celdaAdversario.y=celdaAdversario.y+1;
                                    laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                    laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                                    laberinto.celdas[celdaAdversario.x-1][celdaAdversario.y-1].tipo='V'; 
                                
                                    if(laberinto.lienzoPadre.levelA == 1){
                                        laberinto.lienzoPadre.jugador.traeJugador=true;
                                    }else{
                                        this.traeAdversario=true;
                                    }
                                
                            }
                        }else{
                            laberinto.lateral();
                            /*
                            //FIN DEL MUNDO
                             laberinto.celdas[celdaAdversario.x+1][celdaAdversario.y+1].tipo='V'; 
                             laberinto.celdaPelota.x=laberinto.celdaPelota.x-2;
                             laberinto.celdaPelota.y=laberinto.celdaPelota.y-2;
                             laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';*/
                        }
                    }else{
                        laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='V';
                        celdaAdversario.x=celdaAdversario.x+1;
                        celdaAdversario.y=celdaAdversario.y+1;
                        laberinto.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                    }
                }
            }
        }
    }

    @Override
    public synchronized void run() {
         if(! parar){
          
          buscaPelota.colaEstados.clear();
          buscaPelota.historial.clear();
          buscaPelota.pasos.clear();
          
          llevaPelota.colaEstados.clear();
          llevaPelota.historial.clear();
          llevaPelota.pasos.clear();
          
          Estado subInicial, subObjetivo;
          boolean resultado;
          
            
              subInicial= new Estado(celdaAdversario.x , celdaAdversario.y,'N', null);
             /* subObjetivo = new Estado(adversario.laberinto.lienzoPadre.jugador.celdaJugador.x, 
                            adversario.laberinto.lienzoPadre.jugador.celdaJugador.y,'N', null); */
             subObjetivo = new Estado(laberinto.celdaPelota.x, 
                            laberinto.celdaPelota.y,'N', null); 
             
             System.out.println("subObjetivo Adverssario: " + subObjetivo);
             
             if(traeAdversario == true){
            /* if(laberinto.celdas[adversario.celdaAdversario.x][adversario.celdaAdversario.y] == 
                     laberinto.celdas[adversario.laberinto.celdaPelota.x][adversario.laberinto.celdaPelota.y]){ */
                     
                     if(celdaAdversario.y < 5){
                         gol_abajo=true;
                         subObjetivo = new Estado(0, 5,'N', null);   
                     }
                 
                    switch(celdaAdversario.y){
                        case 5:
                            subObjetivo = new Estado(0, 5,'N', null); 
                            break;
                        case 6:
                            subObjetivo = new Estado(0, 6,'N', null); 
                            break;
                        case 7:
                            subObjetivo = new Estado(0, 7,'N', null); 
                            break;
                        case 8:
                            subObjetivo = new Estado(0, 8,'N', null); 
                            break;
                        case 9:
                            subObjetivo = new Estado(0, 9,'N', null); 
                            break;
                        case 10:
                            subObjetivo = new Estado(0, 10,'N', null); 
                            break;
                        case 11:
                            subObjetivo = new Estado(0, 11,'N', null); 
                            break;     
                    }
                    
                    if(celdaAdversario.y > 11 ){
                        gol_arriba=true;
                        subObjetivo = new Estado(0, 11,'N', null); 
                     } 

                 resultado=llevaPelota.buscar(subInicial, subObjetivo);
                 traeAdversario=false;
             }else{
                 resultado=buscaPelota.buscar(subInicial, subObjetivo);
             }
              
              
             
          if(buscaPelota.pasos.size() > 1 ){
              switch(buscaPelota.pasos.get(1)){
                  case 'D': this.moverCeldaAbajo();break;
                  case 'U': this.moverCeldaArriba();break;
                  case 'R': this.moverCeldaDerecha();break;
                  case 'L': this.moverCeldaIzquierda();break;
                  case 'X': this.moverCeldaArribaDerecha();break;
                  case 'Y': this.moverCeldaArribaIzquierda();break;
                  case 'W': this.moverCeldaAbajoDerecha(); break;
                  case 'Z': this.moverCeldaAbajoIzquierda();break;
              }
              
              laberinto.lienzoPadre.repaint();
          }
          
          if(llevaPelota.pasos.size() > 1 ){
              switch(llevaPelota.pasos.get(1)){
                  case 'D': this.moverCeldaAbajo();break;
                  case 'U': this.moverCeldaArriba();break;
                  case 'R': this.moverCeldaDerecha();break;
                  case 'L': this.moverCeldaIzquierda();break;
                  case 'X': 
                      if(laberinto.lienzoPadre.levelA == 2 ){
                        this.moverCeldaArribaDerecha();
                      }
                      break;
                  case 'Y':
                      if(laberinto.lienzoPadre.levelA == 2 ){
                        this.moverCeldaArribaIzquierda();
                      }
                      break;
                  case 'W': 
                      if(laberinto.lienzoPadre.levelA == 2 ){
                        this.moverCeldaAbajoDerecha();
                      }
                        break;
                  case 'Z': 
                      if(laberinto.lienzoPadre.levelA == 2 ){
                        this.moverCeldaAbajoIzquierda();
                      }
                      break;
              }
              
              laberinto.lienzoPadre.repaint();
          }
          
      }
    }
     
     

    }


