/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego_futbol_ia;

/* paquetes que utilizaremos */
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JOptionPane;



/**
 *
 * @author DANIEL
 */

/* la clase Lienzo hereda de Canvas */

public class Lienzo extends Canvas implements Constantes{
    
    //para pintar el liezo
    public Laberinto laberinto;
    //Imagen de Fondo
    public Image fondo;
    
    //para implementar el doble buffer
    public Graphics graficoBuffer;
    public Image imagenBuffer;
    
    //Para animacion del adversario
    public Adversario adversario;
    public Timer lanzadorTareas;
    
    //mover jugador
    public Jugador jugador;
    
    int minutos=3;
    int segundos=0;
    Timer timer;
    boolean runin;
    
    //Para hacer funcionar el menu configuracion
    int player; 
    int levelA;
    int speed;
    int levelJ;
    
    int num_obstaculos=0;

    //Para usar el pause y continuar
    ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
    

    public Lienzo(int player, int levelA, int speed, int levelJ){
        laberinto=new Laberinto(this);
        
        this.player=player;
        this.levelA=levelA;
        this.speed=speed;
        this.levelJ=levelJ;
        
        //NUMERO DE OBSTACULOS DEPENDIENDO DE LA DIFICULTAD
        System.out.println("Nivel adversario: " + this.levelA);
        switch (this.levelA) {
            case 1: //NIVEL FACIL
                num_obstaculos=0;
                break;
            case 2: //NIVEL DIFICIL
                num_obstaculos=4;
                break;           
            }

        //Para obtener numeros ramdoms de los obstaculos
        Random  rnd1 = new Random();  
        Random  rnd2 = new Random();

        //OBSTACULOS RANDOM
        for(int i=0; i<num_obstaculos; i++){
            laberinto.celdaObstaculos=new Celda((int)(rnd1.nextDouble() * (29 - 2) + 2 ), (int)(rnd2.nextDouble() * (7 - 1) + 1 ),'O');

            laberinto.celdas[laberinto.celdaObstaculos.x][laberinto.celdaObstaculos.y].tipo='O';
        } 

        for(int i=0; i<num_obstaculos; i++){
            laberinto.celdaObstaculos=new Celda((int)(rnd1.nextDouble() * (29 - 2) + 2), (int)(rnd2.nextDouble() * (15 - 9) + 9),'O');

            laberinto.celdas[laberinto.celdaObstaculos.x][laberinto.celdaObstaculos.y].tipo='O';
        } 
        
        //Para animacion adversario
        adversario=new Adversario(laberinto, VELOCIDAD_INICIAL);
        
        //para animacion jugador
        jugador=new Jugador(laberinto, VELOCIDAD_INICIAL);
        
        try {
            fondo = ImageIO.read(new File("Imagenes/fondo2.jpg"));
                } catch (IOException e) {
            System.out.println(e.toString());
            }
        
        Color c1 = new Color(0,116,0);
        //color de fondo
        this.setBackground(c1);
        //dimensiones del lienzo
        this.setSize(laberinto.anchuraLaberinto, laberinto.alturaLaberinto);
        this.laberinto.setName("MEGA SOCCER");
        
        
       //escuchador eventos de teclado
        addKeyListener(new java.awt.event.KeyAdapter() {
            
            @Override
            public void keyPressed(KeyEvent e){
                laberinto.moverCelda(e);
                repaint();
            }
            
           /* @Override
            public void keyReleased(KeyEvent e){
                laberinto.lienzoPadre.jugador.patear();
                repaint();
            }*/
        });  

       //Objetivos de jugador
        //jugador.buscaPelota.destinos.add(new Estado(16,5,'N', null));
        //jugador.buscaPelota.destinos.add(new Estado(2,2,'N', null));
        //jugador.buscaPelota.destinos.add(new Estado(5,8,'N', null));
        
        //adversario.buscaPelota.destinos.add(new Estado(5,10,'N', null));
        
        //Inteligencia sacamos al adversario arriba de esto... 
       // laberinto.lienzoPadre.jugador.buscaPelota.Buscar(5,8,16,8);
        //laberinto.lienzoPadre.jugador.buscaPelota.calcularRuta();
        //lanzadorTareas=new Timer();
        
        
        //Para animacion
        lanzadorTareas=new Timer();
        exec = Executors.newSingleThreadScheduledExecutor();

        //Comienza Jugador y adversario a buscar
        //lanzadorTareas.scheduleAtFixedRate(jugador.buscaPelota,0,500);
        //player = panel.getJugador();
        
        
        
         if(this.player==1){
            System.out.println("Se Juega con el Teclado");
            
            switch (this.speed) {
            case 1:
                exec.scheduleAtFixedRate(adversario,0,800, TimeUnit.MILLISECONDS);
                break;
            case 2:
                exec.scheduleAtFixedRate(adversario,0,500, TimeUnit.MILLISECONDS);
                break;
            case 3:
                exec.scheduleAtFixedRate(adversario,0,200, TimeUnit.MILLISECONDS);
                break;
           
            }
        }else{
            System.out.println("Juega la inteligencia");
            switch (this.speed) {
            case 1:
                exec.scheduleAtFixedRate(jugador, 0, 800, TimeUnit.MILLISECONDS);
                exec.scheduleAtFixedRate(adversario,0,800, TimeUnit.MILLISECONDS);
                break;
            case 2:
                exec.scheduleAtFixedRate(jugador, 0, 500, TimeUnit.MILLISECONDS);
                exec.scheduleAtFixedRate(adversario,0,500, TimeUnit.MILLISECONDS);
                break;
            case 3:
                exec.scheduleAtFixedRate(jugador, 0, 200, TimeUnit.MILLISECONDS);
                exec.scheduleAtFixedRate(adversario,0,200, TimeUnit.MILLISECONDS);
                break;
            }
            
        }
        
        //comienza el cronometro
        this.start(0, 1000); 
    }


    public void pause() {
        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L'; 
        this.exec.shutdown();
    }
    
    public void continuar() {
        exec = Executors.newSingleThreadScheduledExecutor();
        
        if(this.player==1){
            System.out.println("Se Juega con el Teclado");
            
            switch (this.speed) {
            case 1:
                exec.scheduleAtFixedRate(adversario,0,800, TimeUnit.MILLISECONDS);
                break;
            case 2:
                exec.scheduleAtFixedRate(adversario,0,500, TimeUnit.MILLISECONDS);
                break;
            case 3:
                exec.scheduleAtFixedRate(adversario,0,200, TimeUnit.MILLISECONDS);
                break;
           
            }
        }else{
        
            switch (this.speed) {
            case 1:
                exec.scheduleAtFixedRate(jugador, 0, 800, TimeUnit.MILLISECONDS);
                exec.scheduleAtFixedRate(adversario,0,800, TimeUnit.MILLISECONDS);

                break;
            case 2:
                exec.scheduleAtFixedRate(jugador, 0, 500, TimeUnit.MILLISECONDS);
                exec.scheduleAtFixedRate(adversario,0,500, TimeUnit.MILLISECONDS);
                break;
            case 3:
                exec.scheduleAtFixedRate(jugador, 0, 200, TimeUnit.MILLISECONDS);
                exec.scheduleAtFixedRate(adversario,0,200, TimeUnit.MILLISECONDS);
                break;
            }
        }
    }
    
    
    
    TimerTask task = new TimerTask(){
            @Override
            public void run(){
                runin = true;
                if(segundos>0){
                    segundos--;
                }else{
                    segundos = 59;
                    if(minutos > 0 ){
                        minutos--;
                    }else{
                        runin=false;
                        lanzadorTareas.cancel();
                        lanzadorTareas.purge();
                        JOptionPane.showMessageDialog(null,"La Partida termino!");
                    }
                }
            }

            };
    public void start(int timeout, int interval){
        lanzadorTareas.schedule(task, timeout,interval);
    }
    
   
    
    //Llama directamente a paint(), evitando el borrado del componente
    //para omitir el parpadeo
    @Override
    public void update (Graphics g)
   {
       //inicialización del buffer gráfico mediante la imagen
        if(graficoBuffer==null){
            imagenBuffer=createImage(this.getWidth(),this.getHeight());
            graficoBuffer=imagenBuffer.getGraphics();
        }
        
        //volcamos color de fondo e imagen en el nuevo buffer grafico
        graficoBuffer.setColor(getBackground());
        graficoBuffer.fillRect(0,0,this.getWidth(),this.getHeight());
        graficoBuffer.drawImage(fondo, 0, 0, null);
        graficoBuffer.setFont(fuente);
        graficoBuffer.drawString(""+jugador.gol_jugador+" - "+adversario.gol_adversario,648,30);
        graficoBuffer.drawString(minutos+":"+segundos, 650,695 );
        
        //graficoBuffer.drawString(""+jugador.distancia(jugador.jugador.x,adversario.adversario.x),665,30);
        laberinto.update(graficoBuffer);
        
        //pintamos la imagen previa
        g.drawString(minutos+":"+segundos, 650,695 );
        g.drawImage(imagenBuffer, 0, 0, null);
        g.setFont(fuente);
        g.drawString(""+jugador.gol_jugador+" - "+adversario.gol_adversario,648,30);
   }
    
    
    
    //Metodo llamada la primera que se pinta
    @Override
    public void paint(Graphics g){
        update(g);
    }
   
   
   
}
