/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego_futbol_ia;

import java.util.ArrayList;
import java.util.TimerTask;
import static juego_futbol_ia.Constantes.alturaMundoVirtual;
import static juego_futbol_ia.Constantes.anchuraMundoVirtual;

/**
 *
 * @author DANIEL
 */
public class BusquedaProfundidad  implements Constantes {
    
    public Laberinto laberinto;
    public ArrayList<Estado> colaEstados;
    public ArrayList<Estado> historial;
    public ArrayList<Character> pasos;
    public int index_pasos;
    public Estado inicial;
    public Estado objetivo;
    public Estado temp;
    public Boolean exito, ok;
    //para tener un busqueda anchura multiobjetivos
    public ArrayList<Estado> destinos;
    public boolean parar;
    public Adversario adversario;
    public Jugador jugador;
    
    
    
    public BusquedaProfundidad(Laberinto laberinto, Adversario adversario, Jugador jugador){
        this.laberinto=laberinto;
        colaEstados= new ArrayList<>();
        historial = new ArrayList<>();
        pasos = new ArrayList<>();
        index_pasos=0;
        exito=false;
        //inicializacion
        this.adversario=adversario;
        this.jugador=jugador;
        destinos=new ArrayList<>();
        parar=false;
        
        
        
    }
    
    public boolean buscar(Estado inicial, Estado objetivo){
        
        System.out.println("Adversario: Estado Inicial"+inicial.toString());
        System.out.println("Adversario: Estado Objetivo"+objetivo.toString());
        index_pasos=0;
        this.inicial=inicial;
        colaEstados.add(inicial);
        historial.add(inicial);
        this.objetivo=objetivo;
        exito=false;
        
        if(inicial.equals(objetivo)){
            exito=true;
        }
        
        while(!colaEstados.isEmpty() && !exito){
            temp=colaEstados.get(0);
            //System.out.println("Cola Estados"+colaEstados.toString());
            colaEstados.remove(0);

            moverArriba(temp);
            moverAbajo(temp);
            moverDerecha(temp);
            moverIzquierda(temp);
            moverArribaDerecha(temp);
            moverArribaIzquierda(temp);
            moverAbajoDerecha(temp);
            moverAbajoIzquierda(temp); 
             
        }
        
        if(exito){
            System.out.println("Ruta calculada");
            this.calcularRuta();
            return true;
        }else{
            System.out.println("La Ruta no pudo calcularse");
            return false;
        }
        
    }
    
     public double distancia(int x1, int y1, int x2, int y2){
        
        
        double valor;
        double parte1=Math.abs(x1-x2);
       // System.out.println("PARTE 1.1 : "+ parte1);
        //double parte2=Math.pow(Math.abs(y1-y1),2);
        //parte1+=parte2;
        //System.out.println("PARTE 1.2 : "+ parte1);
        //valor=Math.sqrt(parte1);
       // System.out.println("VALOR FUNCION : "+ valor);
        return parte1;
    }

     
    
    private void moverArriba(Estado e) {
        if ( e.y > 0 ) {
            if ( laberinto.celdas[e.x][e.y-1].tipo != 'O' && laberinto.celdas[e.x][e.y-1].tipo != 'J'  ) {
                
                    Estado arriba=new Estado(e.x,e.y-1,'U',e);
                    if ( !historial.contains(arriba)) {
                        colaEstados.add(arriba);
                        historial.add(arriba);
                        //System.out.println("LLEGA arriba");
                        if ( arriba.equals(objetivo)) {
                            objetivo=arriba;
                            exito=true;
                            
                        }
                        
                    }
                
            }
        }
    }//fin del metodo moverArriba
    
    private void moverAbajo(Estado e) {
        if ( e.y < alturaMundoVirtual -2 ) {
            if ( laberinto.celdas[e.x][e.y+1].tipo != 'O' && laberinto.celdas[e.x][e.y+1].tipo != 'J' ) {
                
                    Estado abajo=new Estado(e.x,e.y+1,'D',e);
                    if ( !historial.contains(abajo)) {
                        colaEstados.add(abajo);
                        historial.add(abajo);
                        //System.out.println("LLEGA abajo");
                        if ( abajo.equals(objetivo)) {
                            objetivo=abajo;
                            exito=true;
                            
                        }
                       
                    }
                
            }
        }
    }//fin del metodo moverAbajo
    
    
    private void moverIzquierda(Estado e) {
        if ( e.x > 0 ) {
            

            if ( laberinto.celdas[e.x-1][e.y].tipo != 'O' && laberinto.celdas[e.x-1][e.y].tipo != 'J' ){
                
                    Estado izquierda=new Estado(e.x-1,e.y,'L',e);
                    if ( !historial.contains(izquierda)) {
                        colaEstados.add(izquierda);
                        historial.add(izquierda);
                        //System.out.println("LLEGA izquierda");
                        if ( izquierda.equals(objetivo)) {
                            objetivo=izquierda;
                            exito=true;                            
                        }
                    }
            }
            /*else{
                    int valor = (int)distancia(e.x,e.y,laberinto.celdaJugador.x, laberinto.celdaJugador.y );
                    System.out.println("VALOR: "+valor);
                    if(laberinto.celdas[e.x-valor][e.y].tipo == 'J' ){
                        System.out.println("ESQUIVAR... MOVER LA PELOTA PARA OTRO LADO");
                    }
            }*/
        }
    }// fin del metodo izquierda
    
    private void moverDerecha(Estado e) {
        if ( e.x < anchuraMundoVirtual-1 ) {
            if ( laberinto.celdas[e.x+1][e.y].tipo != 'O' && laberinto.celdas[e.x+1][e.y].tipo != 'J' ) {
                
                    Estado derecha=new Estado(e.x+1,e.y,'R',e);
                    if ( !historial.contains(derecha)){
                        colaEstados.add(derecha);
                        historial.add(derecha);
                        if ( derecha.equals(objetivo)) {
                            objetivo=derecha;
                            exito=true;
                        }
                    }
                
            }
        }
    }// fin del metodo Derecha
    
    private void moverArribaDerecha(Estado e) {
        if ( e.y > 0 && e.x < anchuraMundoVirtual-1 ) {
            if ( laberinto.celdas[e.x+1][e.y-1].tipo != 'O' && laberinto.celdas[e.x+1][e.y-1].tipo != 'J'  ) {
                
                    Estado arribaDerecha=new Estado(e.x+1,e.y-1,'X',e);
                    if ( !historial.contains(arribaDerecha)) {
                        colaEstados.add(arribaDerecha);
                        historial.add(arribaDerecha);
                        //System.out.println("LLEGA arriba");
                        if ( arribaDerecha.equals(objetivo)) {
                            objetivo=arribaDerecha;
                            exito=true;
                        }
                    }
            }
        }
    }
    
    private void moverArribaIzquierda(Estado e) {
        if ( e.y > 0 && e.x > 0 ) {
            if ( laberinto.celdas[e.x-1][e.y-1].tipo != 'O' && laberinto.celdas[e.x-1][e.y-1].tipo != 'J'  ) {
                
                    Estado arribaIzquierda=new Estado(e.x-1,e.y-1,'Y',e);
                    if ( !historial.contains(arribaIzquierda)) {
                        colaEstados.add(arribaIzquierda);
                        historial.add(arribaIzquierda);
                        if ( arribaIzquierda.equals(objetivo)) {
                            objetivo=arribaIzquierda;
                            exito=true;
                        }
                    }
            }
        }
    }
    
       private void moverAbajoDerecha(Estado e) {
        if ( e.y < alturaMundoVirtual -2 && e.x < anchuraMundoVirtual-1 ) {
            if ( laberinto.celdas[e.x+1][e.y+1].tipo != 'O' && laberinto.celdas[e.x+1][e.y+1].tipo != 'J' ) {
                
                    Estado abajoDerecha=new Estado(e.x+1,e.y+1,'W',e);
                    if ( !historial.contains(abajoDerecha)) {
                        colaEstados.add(abajoDerecha);
                        historial.add(abajoDerecha);
                        if ( abajoDerecha.equals(objetivo)) {
                            objetivo=abajoDerecha;
                            exito=true;
                        }
                    }
            }
        }
    }
        private void moverAbajoIzquierda(Estado e) {
        if ( e.y < alturaMundoVirtual -2 && e.x > 0  ) {
            if ( laberinto.celdas[e.x-1][e.y+1].tipo != 'O' && laberinto.celdas[e.x-1][e.y+1].tipo != 'J' ) {
                
                    Estado abajoIzquierda=new Estado(e.x-1,e.y+1,'Z',e);
                    if ( !historial.contains(abajoIzquierda)) {
                        colaEstados.add(abajoIzquierda);
                        historial.add(abajoIzquierda);
                        if ( abajoIzquierda.equals(objetivo)) {
                            objetivo=abajoIzquierda;
                            exito=true;
                        }
                    }
            }
        }
    }
    
    public void calcularRuta() {
        Estado predecesor=objetivo;
        do{
  
            pasos.add(0,predecesor.oper);
            predecesor=predecesor.predecesor;
        }while ( predecesor != null);
        index_pasos=pasos.size()-1;
    }
    



    
    
}
