/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego_futbol_ia;

import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.TimerTask;

/**
 *
 * @author DANIEL
 */
public class BusquedaAnchura implements Constantes{
    public Laberinto laberinto;
    public Queue<Estado> colaEstados;
    public ArrayList<Estado> historial;
    public ArrayList<Character> pasos;
    public int index_pasos;
    public Estado inicial;
    public Estado objetivo;
    public Estado temp;
    public Boolean exito, ok;
    //para tener un busqueda anchura multiobjetivos
    public ArrayList<Estado> destinos;
    public boolean parar;
    public Jugador jugador;
    public Adversario adversario;
    
    public BusquedaAnchura(Laberinto laberinto, Jugador jugador, Adversario adversario){
        this.laberinto=laberinto;
        colaEstados= new PriorityQueue<>();
        historial = new ArrayList<>();
        pasos = new ArrayList<>();
        index_pasos=0;
        exito=false;
        //inicializacion
        this.jugador=jugador;
        this.adversario=adversario;
        destinos=new ArrayList<>();
        parar=false;
        
        
    }
    
    
    public boolean buscar(Estado inicial, Estado objetivo){
        
        inicial.prioridad = distancia(inicial,objetivo);
        
        System.out.println("Jugador: Estado Inicial"+inicial.toString());
        System.out.println("Jugador: Estado Objetivo"+objetivo.toString());
        index_pasos=0;
        
        /*inicial.prioridad= distancia(x1,y1,
                laberinto.lienzoPadre.adversario.celdaAdversario.x,
                laberinto.lienzoPadre.adversario.celdaAdversario.y);*/
        
 
        colaEstados.add(inicial);
        historial.add(inicial);
        this.objetivo=objetivo;
        exito=false;
        
        if(inicial.equals(objetivo)){
            exito=true;
        }
        
        while(!colaEstados.isEmpty() && !exito){
            temp=colaEstados.poll();
           // System.out.println(temp.toString());
            //colaEstados.remove(0);
            
            moverArriba(temp);
            moverAbajo(temp);
            moverDerecha(temp);
            moverIzquierda(temp);
            moverArribaDerecha(temp);
            moverArribaIzquierda(temp);
            moverAbajoDerecha(temp);
            moverAbajoIzquierda(temp); 
        }
        
        if(exito){
            System.out.println("Ruta calculada");
            this.calcularRuta();
            return true;
        }else{
            System.out.println("La Ruta no pudo calcularse");
            return false;
        }
        
    }
    //dame estado adversario
    
    
    //Distancia adversario
    //public double distancia(int x1, int y1, int x2, int y2){
    public double distancia(Estado inicial, Estado objetivo){
        
        
        double valor;
        double parte1=Math.pow(Math.abs(inicial.x-objetivo.x),2);
        double parte2=Math.pow(Math.abs(inicial.y-objetivo.y),2);
        parte1+=parte2;
        valor=Math.sqrt(parte1);
        return valor;
    }
    
     public double distancia2(int x1, int y1, int x2, int y2){
        
        
        double valor;
        double parte1=Math.abs(x1-x2);
       // System.out.println("PARTE 1.1 : "+ parte1);
        //double parte2=Math.pow(Math.abs(y1-y1),2);
        //parte1+=parte2;
        //System.out.println("PARTE 1.2 : "+ parte1);
        //valor=Math.sqrt(parte1);
       // System.out.println("VALOR FUNCION : "+ valor);
        return parte1;
    }
    
    private void moverArriba(Estado e) {
        if ( e.y > 0 ) {
            if ( laberinto.celdas[e.x][e.y-1].tipo != 'O' && laberinto.celdas[e.x][e.y-1].tipo != 'A' ) {
                
                    Estado arriba=new Estado(e.x,e.y-1,'U',e);
                    arriba.prioridad= distancia(arriba, objetivo);
                    
                    if ( !historial.contains(arriba)) {
                        colaEstados.add(arriba);
                        historial.add(arriba);
                        if ( arriba.equals(objetivo)) {
                            objetivo=arriba;
                            exito=true;
                        }
                    }
                
            }
        }
    }//fin del metodo moverArriba
    
    private void moverAbajo(Estado e) {
        if ( e.y+1 < alturaMundoVirtual ) {
            if ( laberinto.celdas[e.x][e.y+1].tipo != 'O' && laberinto.celdas[e.x][e.y+1].tipo != 'A' ) {
                
                    Estado abajo=new Estado(e.x,e.y+1,'D',e);
                    abajo.prioridad= distancia(abajo, objetivo);
                    if ( !historial.contains(abajo)) {
                        colaEstados.add(abajo);
                        historial.add(abajo);
                        if ( abajo.equals(objetivo)) {
                            objetivo=abajo;
                            exito=true;
                        }
                    }
                
            }
        }
    }//fin del metodo moverAbajo
    
    
    private void moverIzquierda(Estado e) {
        if ( e.x > 0 ) {
            if ( laberinto.celdas[e.x-1][e.y].tipo != 'O' && laberinto.celdas[e.x-1][e.y].tipo != 'A' ) {
                
                    Estado izquierda=new Estado(e.x-1,e.y,'L',e);
                    izquierda.prioridad= distancia(izquierda, objetivo);
                    if ( !historial.contains(izquierda)) {
                        colaEstados.add(izquierda);
                        historial.add(izquierda);
                        if ( izquierda.equals(objetivo)) {
                            objetivo=izquierda;
                            exito=true;
                        }
                    }
                
            }
        }
    }// fin del metodo izquierda
    
    private void moverDerecha(Estado e) {
        if ( e.x < anchuraMundoVirtual-1 ) {
            if ( laberinto.celdas[e.x+1][e.y].tipo != 'O' && laberinto.celdas[e.x+1][e.y].tipo != 'A' ) {
                
                    Estado derecha=new Estado(e.x+1,e.y,'R',e);
                    derecha.prioridad= distancia(derecha, objetivo);
                    if ( !historial.contains(derecha)){
                        colaEstados.add(derecha);
                        historial.add(derecha);
                        if ( derecha.equals(objetivo)) {
                            objetivo=derecha;
                            exito=true;
                        }
                    }
                
            }
        }
    }// fin del metodo Derecha
    
     private void moverArribaDerecha(Estado e) {
        if ( e.y > 0 && e.x < anchuraMundoVirtual-1 ) {
            if ( laberinto.celdas[e.x+1][e.y-1].tipo != 'O' ) {
                
                    Estado arribaDerecha=new Estado(e.x+1,e.y-1,'X',e);
                    arribaDerecha.prioridad= distancia(arribaDerecha, objetivo);
                    if ( !historial.contains(arribaDerecha)) {
                        colaEstados.add(arribaDerecha);
                        historial.add(arribaDerecha);
                        //System.out.println("LLEGA arriba");
                        if ( arribaDerecha.equals(objetivo)) {
                            objetivo=arribaDerecha;
                            exito=true;
                        }
                    }
            }
        }
    }
    
    private void moverArribaIzquierda(Estado e) {
        if ( e.y > 0 && e.x > 0 ) {
            if ( laberinto.celdas[e.x-1][e.y-1].tipo != 'O'  ) {
                
                    Estado arribaIzquierda=new Estado(e.x-1,e.y-1,'Y',e);
                    arribaIzquierda.prioridad= distancia(arribaIzquierda, objetivo);
                    if ( !historial.contains(arribaIzquierda)) {
                        colaEstados.add(arribaIzquierda);
                        historial.add(arribaIzquierda);
                        if ( arribaIzquierda.equals(objetivo)) {
                            objetivo=arribaIzquierda;
                            exito=true;
                        }
                    }
            }
        }
    }
    
       private void moverAbajoDerecha(Estado e) {
        if ( e.y < alturaMundoVirtual -2 && e.x < anchuraMundoVirtual-1 ) {
            if ( laberinto.celdas[e.x+1][e.y+1].tipo != 'O' ) {
                
                    Estado abajoDerecha=new Estado(e.x+1,e.y+1,'W',e);
                    abajoDerecha.prioridad= distancia(abajoDerecha, objetivo);
                    if ( !historial.contains(abajoDerecha)) {
                        colaEstados.add(abajoDerecha);
                        historial.add(abajoDerecha);
                        if ( abajoDerecha.equals(objetivo)) {
                            objetivo=abajoDerecha;
                            exito=true;
                        }
                    }
            }
        }
    }
        private void moverAbajoIzquierda(Estado e) {
        if ( e.y < alturaMundoVirtual -2 && e.x > 0  ) {
            if ( laberinto.celdas[e.x-1][e.y+1].tipo != 'O' ) {
                
                    Estado abajoIzquierda=new Estado(e.x-1,e.y+1,'Z',e);
                    abajoIzquierda.prioridad= distancia(abajoIzquierda, objetivo);
                    if ( !historial.contains(abajoIzquierda)) {
                        colaEstados.add(abajoIzquierda);
                        historial.add(abajoIzquierda);
                        if ( abajoIzquierda.equals(objetivo)) {
                            objetivo=abajoIzquierda;
                            exito=true;
                        }
                    }
            }
        }
    }
    
    public void calcularRuta() {
        Estado predecesor=objetivo;
        do{
  
            pasos.add(0,predecesor.oper);
            predecesor=predecesor.predecesor;
        }while ( predecesor != null);
        index_pasos=pasos.size()-1;
    }
    




    
    
}
