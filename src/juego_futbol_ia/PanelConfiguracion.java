/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego_futbol_ia;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.ChangeEvent;

import static juego_futbol_ia.Constantes.RUTA;

/**
 *
 * @author DANIEL
 */
public class PanelConfiguracion extends JPanel implements Constantes {
  /*  public JLabel velocidad;
    public JSlider cambiarVelocidad; 
    public PanelDeJuego panelJuego;*/
    
    public JButton comenzar;
    JCheckBox with_feedback, with_feedback2;
    JComboBox jugador, nivelA, velocidad, nivelJ;
    public JLabel nombre;
    public JTextField campo_nombre;
    public ImageIcon icono;
    public String[] tipos_letra;
    public JFrame ventana_principal;
    public boolean toShow_feedback;
    JLabel txt_nivelA, txt_nivelJ, txt_velocidad,txt_jugador;
    int player, levelA, speed, levelJ;
    String texto;
    
    public PanelConfiguracion(JFrame ventana_principal){
        
        this.ventana_principal = ventana_principal;
        this.setName("MEGA FUTBOL - CONFIGURACION");
        this.setLayout(null);
        this.setSize(SCREEN_SIZE.width, SCREEN_SIZE.width);
        toShow_feedback=true;
        
                
        String [] nivelesA = {"1: Facil", "2: Dificil"};
        nivelA = new JComboBox(nivelesA);
        txt_nivelA = new JLabel("Seleccione la dificultad del Juego:");
        txt_nivelA.setBounds(30, 50,300,30);
        txt_nivelA.setForeground(Color.WHITE);
        txt_nivelA.setVisible(true);
        add(txt_nivelA);
        nivelA.setBounds(30, 90,200,30);
        add(nivelA);
        
        
        String [] jugadores = {"Jugador Vs CPU", "CPU Vs CPU"};
        jugador = new JComboBox(jugadores);
        txt_jugador = new JLabel("Seleccione el tipo de juego:");
        txt_jugador.setBounds(30,150,200, 30);
        txt_jugador.setForeground(Color.WHITE);
        txt_jugador.setVisible(true);
        add(txt_jugador);
        jugador.setBounds(30,190,200,30);
        add(jugador);
        
        
        
        String [] nivelesJ = {"1: Facil", "2: Dificil"};
        nivelJ = new JComboBox(nivelesJ);
        txt_nivelJ = new JLabel("Seleccione la dificultad del Jugador:");
        txt_nivelJ.setBounds(290, 150,300,30);
        txt_nivelJ.setForeground(Color.WHITE);
        txt_nivelJ.setVisible(true);
        add(txt_nivelJ);
        nivelJ.setBounds(290, 190,200,30);
        nivelJ.setEnabled(false);
        add(nivelJ); 
        
         // Accion a realizar cuando el JComboBox cambia de item seleccionado.
		jugador.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
                            texto =jugador.getSelectedItem().toString();
                            if(texto.equals("CPU Vs CPU")){
                                nivelJ.setEnabled(true);
                            }
			}
		});

        String [] velocidades = {"1: Lento", "2: Normal", "3: Rapido"};
        velocidad = new JComboBox(velocidades);
        txt_velocidad = new JLabel("Seleccione la velocidad:");
        txt_velocidad.setBounds(30,250,200, 30);
        txt_velocidad.setForeground(Color.WHITE);
        txt_velocidad.setVisible(true);
        add(txt_velocidad);
        velocidad.setBounds(30, 290, 200, 30);
        add(velocidad);


        comenzar=new JButton("GO!");
        comenzar.setBounds(1000, 550, 310, 100);
        comenzar.setOpaque(true);
        comenzar.setFont(new Font("Times New Roman",Font.BOLD,20));
        comenzar.setBackground(Color.BLUE);
        comenzar.setForeground(Color.WHITE);
        comenzar.setHorizontalTextPosition(JButton.CENTER);
        comenzar.setVerticalTextPosition(JButton.CENTER);
        comenzar.addActionListener(this::pulsarBotonComenzar);

        
        add(comenzar);
        //add(with_feedback);
     /*   this.panelJuego=panelJuego;
        
        velocidad= new JLabel();
        velocidad.setForeground(Color.yellow);
        velocidad.setFont(fuente);
        
        cambiarVelocidad= new JSlider(JSlider.VERTICAL, VELOCIDAD_MAXIMA, VELOCIDAD_MINIMA, VELOCIDAD_INICIAL);
        
        cambiarVelocidad.addChangeListener(this::escuchadorSlider);
        cambiarVelocidad.setMajorTickSpacing(100);
        cambiarVelocidad.setPaintTicks(true);
        
        this.setBackground(Color.gray);
        this.setLayout(new BorderLayout());
        add(velocidad, BorderLayout.WEST);
        add(cambiarVelocidad, BorderLayout.CENTER);*/
     
        ventana_principal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    public void pulsarBotonComenzar(ActionEvent e){
        player=jugador.getSelectedIndex()+1;
        levelA=nivelA.getSelectedIndex()+1;
        speed=velocidad.getSelectedIndex()+1;
        
        if(player == 2){
            levelJ=nivelJ.getSelectedIndex()+1;
            VentanaPrincipal ventanaPrincipal = new VentanaPrincipal(player, levelA, speed, levelJ);
            ventanaPrincipal.setVisible(true);
            ventana_principal.setVisible(false); 
            ventana_principal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        }else{
            VentanaPrincipal ventanaPrincipal = new VentanaPrincipal(player, levelA, speed, 0);
            ventanaPrincipal.setVisible(true);
            ventana_principal.setVisible(false); 
            ventana_principal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        }

    }

    @Override
    public void paintComponent (Graphics g){
        Dimension d =getSize();
        BufferedImage fondo = null;
        try{
            fondo = ImageIO.read(new File("Imagenes/fondo3.jpg"));
            //fondo=ImageIO.read(new File(RUTA_DIRECTORIO+"\\Imagenes\\fondo3.jpg"));
        }catch(IOException ex){
            Logger.getLogger(PanelMenuInicial.class.getName()).log(Level.SEVERE, null, ex);
        }
        g.drawImage(fondo, 0, 0, d.width, d.height, null);
        super.paintComponents(g);
    }

    
}
