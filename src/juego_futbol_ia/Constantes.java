/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego_futbol_ia;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.util.Random;
import static javafx.scene.text.Font.font;


/**
 *
 * @author DANIEL
 */

/* interfaz constantes para variables de configuracion globales */

public interface Constantes {
    public final int anchuraCelda=40;
    public final int alturaCelda=40;
    public final int anchuraMundoVirtual=33;
    public final int alturaMundoVirtual=17;
    
    //Para manejar los tipos de celdas
    public final char JUGADOR = 'J';
    public final char CAMINO = 'V';
    public final char OBSTACULO= 'O';
    public final char ADVERSARIO= 'A';
    public final char PATEAR= 'P';
    public final char PELOTA= 'L';
    public final char ARCO= 'C';
    
    //PARA PANEL DE CONFIGURACION
    public final int VELOCIDAD_MAXIMA=300;
    public final int VELOCIDAD_MINIMA=2000;
    public final int VELOCIDAD_INICIAL=1000;
    
    public final Font fuente= new Font("Times New Roman",Font.BOLD,32);
    
    //Para imagen de fondo
    public final int ALFA=127;
    public final Color COLORFONDO=new Color(0,116,0,ALFA);
    
    //Ruta del proyecto
    //public final String RUTA=System.getProperty( "user.dir" );
    public final String RUTA="file:///"+System.getProperty( "user.dir" );

    
    //Para pantalla de bienvenida
    public int FUENTE_SIZE=12;
    public int CELDA_SIZE=25;
    public int N=31;
    public int M=21;
    public Dimension SCREEN_SIZE = Toolkit.getDefaultToolkit().getScreenSize();
    public String RUTA_DIRECTORIO = System.getProperty("user.dir");
    
    //para tener un numero aleatorio
    
    default int numeroAleatorio(int minimo, int maximo) {
        Random random = new Random();
        int numero_aleatorio = random.nextInt((maximo - minimo) + 1) + minimo;
        return numero_aleatorio;
}

}
