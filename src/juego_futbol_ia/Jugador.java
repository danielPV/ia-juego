/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego_futbol_ia;

import java.util.Random;
import java.util.TimerTask;


/**
 *
 * @author DANIEL
 */
public class Jugador extends TimerTask implements Constantes{
    
    public Laberinto laberinto;
    public Celda celdaJugador;
    
    //Cuando se agarra la pelota
    boolean agarrado=false;
    
    //Para patear
    boolean up=false;
    boolean down=false;
    boolean right=false;
    boolean left=false;
    
    //Para animacion personaje
  /*  int arriba=0;
    int abajo=2;
    int derecha=3;
    int izquierda=1;*/
    //animacion pelota
    int rueda=0;
    int gol_jugador=0;
    boolean gol=false;
    boolean traeJugador=false;
    
    int valor, valor2;
    
    public BusquedaProfundidad buscaPelota;
    public BusquedaAnchura llevaPelota;
    
    public boolean parar;
    
    public boolean gol_arriba=false;
    public boolean gol_abajo=false;
    
   
    
    
    public Jugador(Laberinto laberinto, int tiempoDormido){
      
        this.laberinto=laberinto;
        celdaJugador=new Celda(5, 8,'J');

        laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
        llevaPelota=new BusquedaAnchura(laberinto, this, laberinto.lienzoPadre.adversario);
        buscaPelota= new BusquedaProfundidad(laberinto, laberinto.lienzoPadre.adversario, this);
        
    }
    
    public void acomodarPelota(int x, int y){
         //SI NO HAY OBSTACULOS ARRIBA
        if(laberinto.celdas[x+1][y-1].tipo != 'O'){
            laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
            laberinto.celdaPelota.y=laberinto.celdaPelota.y-1;
            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo = 'L';
            laberinto.celdas[celdaJugador.x+1][celdaJugador.y].tipo = 'V';
        }else{
            laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
            laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo = 'L';
            laberinto.celdas[celdaJugador.x+1][celdaJugador.y].tipo = 'V';
        }

    }

        public void moverCeldaArriba(){
        //agregue para que no se coma al adversario lo final
        if (celdaJugador.y > 1  ) {
           
            if(laberinto.celdas[celdaJugador.x][celdaJugador.y-1].tipo != 'A'){
                if(laberinto.celdas[celdaJugador.x][celdaJugador.y-1].tipo != 'O'){
                    if(laberinto.celdas[celdaJugador.x][celdaJugador.y-1].tipo == 'L'){
                        if(celdaJugador.y-3 >= 0 ){
                            if(laberinto.celdas[celdaJugador.x][celdaJugador.y-2].tipo != 'A'){
                                 if(laberinto.celdas[celdaJugador.x][celdaJugador.y-2].tipo != 'O'){
                                     
                                     if(laberinto.lienzoPadre.player == 1){
                                           laberinto.celdaPelota.y=laberinto.celdaPelota.y-1;
                                           laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                           //celdaJugador.y=celdaJugador.y-1;
                                           //laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                                           laberinto.celdas[celdaJugador.x][celdaJugador.y-1].tipo='V';
                                           laberinto.celdas[celdaJugador.x][celdaJugador.y].indexSprite=laberinto.arriba;
                                           traeJugador=true;
                                     }else{                                       
                                        if(laberinto.celdas[celdaJugador.x-1][celdaJugador.y-1].tipo == 'A' || laberinto.celdas[celdaJugador.x-1][celdaJugador.y-1].tipo == 'O'){
                                            laberinto.celdaPelota.y=laberinto.celdaPelota.y-1;
                                        }else{
                                            laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                        }
                                        //MUEVE LA PELOTA HACIA ARRIBA
                                        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                        celdaJugador.y=celdaJugador.y-1;
                                        laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                                        laberinto.celdas[celdaJugador.x][celdaJugador.y+1].tipo='V';
                                        laberinto.celdas[celdaJugador.x][celdaJugador.y].indexSprite=laberinto.arriba;
                                        traeJugador=true;
                                       
                                     } 
                            }else{
                                     //ESQUIVA OBSTACULO
                                    switch(laberinto.lienzoPadre.levelJ){
                                    case 1: //FACIL
                                        celdaJugador.y=celdaJugador.y-1;
                                        laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J'; 
                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                        break;
                                    case 2://DIFICIL
                                        moverCeldaArribaIzquierda();
                                        break;
                                    }
                                 }
                         }else{
                                //SE ENCUENTRA CON ADVERSARIO
                                switch(laberinto.lienzoPadre.levelJ){
                                    case 1: //FACIL
                                        if(laberinto.celdas[celdaJugador.x-1][celdaJugador.y].tipo != 'O' ){
                                            moverCeldaIzquierda();
                                            if(laberinto.celdas[celdaJugador.x+1][celdaJugador.y].tipo != 'O'){
                                                moverCeldaArriba();
                                            }else{
                                                moverCeldaAbajoIzquierda();
                                            }
                                        }else{
                                            if(laberinto.celdas[celdaJugador.x-1][celdaJugador.y-1].tipo != 'O'){
                                                moverCeldaArribaIzquierda();
                                            }else{
                                                if(laberinto.celdas[celdaJugador.x+1][celdaJugador.y-1].tipo != 'O'){
                                                    moverCeldaArribaDerecha();
                                                }
                                            }
                                            
                                        }
                                        break;
                                    case 2://DIFICIL
                                        laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                        break;
                                }
                                
                            }
                        }else{
                            
                            laberinto.lateral();
                            /*
                            //FINAL DE LA CANCHA
                            if(laberinto.lienzoPadre.player == 1){
                                celdaJugador.y=celdaJugador.y-1;
                                laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L'; 
                                laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                            }else{
                                switch(laberinto.lienzoPadre.levelJ){
                                    case 1:
                                        celdaJugador.y=celdaJugador.y-1;
                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                         laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L'; 
                                         laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                                        break;
                                    case 2:
                                         if(laberinto.celdas[celdaJugador.x+1][celdaJugador.y+1].tipo == 'A' || laberinto.celdas[celdaJugador.x+1][celdaJugador.y+1].tipo == 'O'){
                                             laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                             laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                         }else{
                                             laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                             laberinto.celdaPelota.y=laberinto.celdaPelota.y+2;
                                             laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                         }
                                        break;
                                }   
                            }
                            
                            */
                        }
               }else{
                   //SE MUEVE ARRIBA
                    laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='V';
                    celdaJugador.y=celdaJugador.y-1;
                    laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                    laberinto.celdas[celdaJugador.x][celdaJugador.y].indexSprite=laberinto.arriba;
               }
           }
        }
            /*
            if(celdas[celdaJugador.x][celdaJugador.y] == celdas[celdaPelota.x][celdaPelota.y+1]){
                celdaPelota.y=celdaJugador.y-2;
                celdas[celdaPelota.x][celdaPelota.y].indexSpritePelota=1;
                agarrado=true;
                
            }*/
            
            //aca para que la pelota no se coma al adversario pero despues se quita eso para que el adversario pueda agarrar la pelota
         /*   if(agarrado == true ){
                celdas[celdaPelota.x][celdaPelota.y].tipo='V';
                celdaPelota.y=celdaJugador.y-2;
                celdaPelota.x=celdaJugador.x;
                celdas[celdaPelota.x][celdaPelota.y].tipo='L';
                celdas[celdaPelota.x][celdaPelota.y].indexSpritePelota=rueda;
                celdas[celdaPelota.x][celdaPelota.y+1].tipo='V';
                
                up=true;
                down=false;
                right=false;
                left=false;
                
            } */
  
            
        
        }
    }
    
        public void moverCeldaAbajo(){
        if (celdaJugador.y < alturaMundoVirtual -2 ) {
            
            if(laberinto.celdas[celdaJugador.x][celdaJugador.y+1].tipo != 'A'){
                if(laberinto.celdas[celdaJugador.x][celdaJugador.y+1].tipo != 'O'){
                    if(laberinto.celdas[celdaJugador.x][celdaJugador.y+1].tipo == 'L'){
                        if(celdaJugador.y+3 < alturaMundoVirtual){
                            if(laberinto.celdas[celdaJugador.x][celdaJugador.y+2].tipo != 'A'){
                                if(laberinto.celdas[celdaJugador.x][celdaJugador.y+2].tipo != 'O'){
                                    
                                    if(laberinto.lienzoPadre.player == 1){
                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                        celdaJugador.y=celdaJugador.y+1;
                                        laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                                        laberinto.celdas[celdaJugador.x][celdaJugador.y-1].tipo='V';
                                         laberinto.celdas[celdaJugador.x][celdaJugador.y].indexSprite=laberinto.abajo;
                                        traeJugador=true;
                                    }else{
                                        //MUEVE LA PELOTA HACIA ABAJO
                                        //PATEA LA PELOTA
                                        if(laberinto.celdas[celdaJugador.x+1][celdaJugador.y+1].tipo == 'J' || laberinto.celdas[celdaJugador.x+1][celdaJugador.y+1].tipo == 'O'){
                                                laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                        }else{
                                                laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                        }
                                        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                        celdaJugador.y=celdaJugador.y+1;
                                        laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                                        laberinto.celdas[celdaJugador.x][celdaJugador.y-1].tipo='V';
                                         laberinto.celdas[celdaJugador.x][celdaJugador.y].indexSprite=laberinto.abajo;
                                        traeJugador=true;
                                
                                    }
                                }else{
                                    //SE PILLA CON UN OBSTACULO
                                   switch(laberinto.lienzoPadre.levelJ){
                                    case 1: //FACIL
                                        celdaJugador.y=celdaJugador.y+1;
                                        laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J'; 
                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y-1;
                                        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                        break;
                                    case 2://DIFICIL
                                        moverCeldaAbajoIzquierda();
                                        break;
                                    }
                                }
                            }else{
                                //SE ENCUENTRA CON ADVERSARIO
                                switch(laberinto.lienzoPadre.levelJ){
                                    case 1: //FACIL
                                        moverCeldaIzquierda();
                                        moverCeldaAbajo();
                                        break;
                                    case 2://DIFICIL
                                        laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y-1;
                                        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                        break;
                                }
                            }

                        }else{
                            laberinto.lateral();
                            /*
                            //LLEGA AL FINAL DE LA CANCHA
                            if(laberinto.lienzoPadre.player == 1){
                                celdaJugador.y=celdaJugador.y+1;
                                laberinto.celdaPelota.y=laberinto.celdaPelota.y-1;
                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J'; 
                            }else{
                                switch(laberinto.lienzoPadre.levelJ){
                                    case 1:
                                        celdaJugador.y=celdaJugador.y+1;
                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y-1;
                                        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                        laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J'; 
                                        break;
                                    case 2:
                                        if(laberinto.celdas[celdaJugador.x+1][celdaJugador.y-1].tipo == 'A' || laberinto.celdas[celdaJugador.x+1][celdaJugador.y-1].tipo == 'O'){
                                             laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                             laberinto.celdaPelota.y=laberinto.celdaPelota.y-1;
                                            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                         }else{
                                             laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                             laberinto.celdaPelota.y=laberinto.celdaPelota.y-2;
                                             laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                         }
                                        break;
                                }
                            } */
                        }
                    }else{
                        laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='V';
                        celdaJugador.y=celdaJugador.y+1;
                        laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                        laberinto.celdas[celdaJugador.x][celdaJugador.y].indexSprite=laberinto.abajo;
   

                    }
                }
        }
          /*  if(celdas[celdaJugador.x][celdaJugador.y] == celdas[celdaPelota.x][celdaPelota.y-1]){
                celdaPelota.y=celdaJugador.y+2;
                celdas[celdaPelota.x][celdaPelota.y].indexSpritePelota=1;
                agarrado=true;
            }
            
            if(agarrado == true){
                celdas[celdaPelota.x][celdaPelota.y].tipo='V';
                celdaPelota.y=celdaJugador.y+2;
                celdaPelota.x=celdaJugador.x;
                celdas[celdaPelota.x][celdaPelota.y].tipo='L';
                celdas[celdaPelota.x][celdaPelota.y].indexSpritePelota=rueda;
                celdas[celdaPelota.x][celdaPelota.y-1].tipo='V';
                
                up=false;
                down=true;
                right=false;
                left=false;
            } 
            
            celdas[celdaJugador.x][celdaJugador.y].tipo='V';
            celdaJugador.y=celdaJugador.y+1;
            celdas[celdaJugador.x][celdaJugador.y].tipo='J';
            celdas[celdaJugador.x][celdaJugador.y].indexSprite=abajo;
            
            if(abajo == 14){
                abajo=2;
            }
            
            if(rueda >= 2){
                rueda=0;
            }*/
        }
    }
    
    public void moverCeldaIzquierda(){
        if (celdaJugador.x > 0 ) {
            
            if(laberinto.celdas[celdaJugador.x-1][celdaJugador.y].tipo!='A'){
                if(laberinto.celdas[celdaJugador.x-1][celdaJugador.y].tipo!='O'){
                    if(laberinto.celdas[celdaJugador.x-1][celdaJugador.y].tipo == 'L'){
                        
                        if(celdaJugador.x-2 >= 0 ){
                            if(laberinto.celdas[celdaJugador.x-2][celdaJugador.y].tipo!='A'){
                                if(laberinto.celdas[celdaJugador.x-2][celdaJugador.y].tipo!='O'){
                                    
                                    
                                    if(laberinto.lienzoPadre.player == 1){
                                        laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                        celdaJugador.x=celdaJugador.x-1;
                                        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                        laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                                        laberinto.celdas[celdaJugador.x+1][celdaJugador.y].tipo='V';
                                        laberinto.celdas[celdaJugador.x][celdaJugador.y].indexSprite=laberinto.izquierda;
                                        traeJugador=true;
                                    }else{
                                        switch(laberinto.lienzoPadre.levelJ){
                                            case 1:
                                                if(laberinto.celdas[celdaJugador.x-1][celdaJugador.y-1].tipo == 'A' || laberinto.celdas[celdaJugador.x-1][celdaJugador.y-1].tipo == 'O'){
                                                    moverCeldaArribaIzquierda();
                                                    traeJugador=true;
                                                }else{
                                                    moverCeldaAbajoIzquierda();
                                                    traeJugador=true;
                                                }
                                                break;
                                            case 2:
                                                 //EL JUGADOR SE ACOMODA LA PELOTA A LA DERECHA
                                                laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                                celdaJugador.x=celdaJugador.x-1;
                                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                                laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                                                
                                                traeJugador=true;
                                                break;
                                        }
                                    }
                                    
                                    
                                }else{
                                   //EL JUGADOR SE ACOMODA LA PELOTA A LA DERECHA
                                   laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                   celdaJugador.x=celdaJugador.x-1;
                                   laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                   laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                                   traeJugador=true;  
                                }
                            }else{
                                
                                celdaJugador.x=celdaJugador.x-1;
                                laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                                laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                       
                            }
                        }else{
                            
                            //AL FINAL DE LA CHANCHA
                            laberinto.celdas[celdaJugador.x-1][celdaJugador.y].tipo='V'; 
                            laberinto.celdaPelota.x=laberinto.celdaPelota.x+2;
                            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                        }
                    }else{
                        //EL JUGADOR SE MUEVE A LA IZQUIERDA
                        laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='V';
                        celdaJugador.x=celdaJugador.x-1;
                        laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                        laberinto.celdas[celdaJugador.x][celdaJugador.y].indexSprite=laberinto.izquierda;


                    }
                }
        }
         /*
            if(celdas[celdaJugador.x][celdaJugador.y] == celdas[celdaPelota.x+1][celdaPelota.y]){
                celdaPelota.x=celdaJugador.x-2; 
                agarrado=true;
            }
            
            if(agarrado == true){
                celdas[celdaPelota.x][celdaPelota.y].tipo='V';
                celdaPelota.x=celdaJugador.x-2;
                celdaPelota.y=celdaJugador.y;
                celdas[celdaPelota.x][celdaPelota.y].tipo='L';
                celdas[celdaPelota.x][celdaPelota.y].indexSpritePelota=rueda;
               // celdas[celdaPelota.x][celdaPelota.y].tipo='V';
               
                up=false;
                down=false;
                right=false;
                left=true;
                
            }  
            
            celdas[celdaJugador.x][celdaJugador.y].tipo='V';
            celdaJugador.x=celdaJugador.x-1;
            celdas[celdaJugador.x][celdaJugador.y].tipo='J';
            celdas[celdaJugador.x][celdaJugador.y].indexSprite=izquierda;
            
            if(izquierda == 13){
                izquierda=1;
            }
            
            if(rueda >= 2){
                rueda=0;
            }*/
            
        }
    }

    public void moverCeldaDerecha(){
        if ((celdaJugador.x < anchuraMundoVirtual -1)) {
            
            if(laberinto.celdas[celdaJugador.x+1][celdaJugador.y].tipo!='A'){ //SI ESTA AL LADO EL ADVERSARIO
                if(laberinto.celdas[celdaJugador.x+1][celdaJugador.y].tipo!='O'){ //SI HAY UN OBSTACULO
                    
                    if(laberinto.celdas[celdaJugador.x+1][celdaJugador.y].tipo == 'L'){ 
                        if(celdaJugador.x+1 < anchuraMundoVirtual-1){ // SI ESTA AL FINAL DE LA CANCHA
                            if(laberinto.lienzoPadre.levelJ == 1 || laberinto.lienzoPadre.levelJ == 0 ){
                                valor=2;
                                valor2=2;
                            }else if(laberinto.lienzoPadre.levelJ == 2){
                                valor=Math.abs((int) laberinto.lienzoPadre.jugador.llevaPelota.distancia2(celdaJugador.x, celdaJugador.y, laberinto.lienzoPadre.adversario.celdaAdversario.x, laberinto.lienzoPadre.adversario.celdaAdversario.y));  
                                valor2=laberinto.calculaObstaculosJ(celdaJugador.x, celdaJugador.y);
                            }
                            
                            if(laberinto.celdas[Math.abs(celdaJugador.x+valor)][celdaJugador.y].tipo!='A' ){ // SI ESTOY CON PELOTA Y NO ESTA EL ADVERSARIO AL LADO
                                if(laberinto.celdas[Math.abs(celdaJugador.x+valor2)][celdaJugador.y].tipo!='O'){ // SI ESTOY CON PELOTA Y NO VIENE UN OBSTACULO

                                    //SI LA PELOTA ENTRA AL ARCO
                                    if((laberinto.celdas[celdaJugador.x][celdaJugador.y] == laberinto.celdas[31][5]) ||
                                                         (laberinto.celdas[celdaJugador.x][celdaJugador.y] == laberinto.celdas[31][6]) ||
                                                         (laberinto.celdas[celdaJugador.x][celdaJugador.y] == laberinto.celdas[31][7]) ||
                                                         (laberinto.celdas[celdaJugador.x][celdaJugador.y] == laberinto.celdas[31][8]) ||
                                                         (laberinto.celdas[celdaJugador.x][celdaJugador.y] == laberinto.celdas[31][9]) ||
                                                         (laberinto.celdas[celdaJugador.x][celdaJugador.y] == laberinto.celdas[31][10] ||
                                                         (laberinto.celdas[celdaJugador.x][celdaJugador.y] == laberinto.celdas[31][11]) ) ){

                                            System.out.println("gol del Jugador");
                                            laberinto.lienzoPadre.jugador.gol_jugador=laberinto.lienzoPadre.jugador.gol_jugador+1;
                                            //laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';  
                                            laberinto.celdas[celdaJugador.x+1][celdaJugador.y].tipo='V';   
                                            laberinto.gol();
                                        }else{
                                            //-------------------------- HACER QUE VAYA AL ARCO
                                            if(gol_abajo== true){
                                                if(laberinto.celdas[celdaJugador.x+2][celdaJugador.y+1].tipo != 'A'){
                                                    if(laberinto.celdas[celdaJugador.x+2][celdaJugador.y+1].tipo != 'O'){
                                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                                        laberinto.celdas[celdaJugador.x+1][celdaJugador.y].tipo='V';
                                                        traeJugador = true;
                                                        gol_abajo=false; 
                                                    }
                                                    
                                                }
                                            }
                                            
                                            if(gol_arriba== true){
                                                if(laberinto.celdas[celdaJugador.x+2][celdaJugador.y-1].tipo != 'A'){
                                                    if(laberinto.celdas[celdaJugador.x+2][celdaJugador.y-1].tipo != 'O'){
                                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y-1;
                                                        laberinto.celdas[celdaJugador.x+1][celdaJugador.y].tipo='V';
                                                        traeJugador = true;
                                                        gol_arriba=false; 
                                                    }
                                                }
                                            }
                                            //EL JUGADOR MUEVE LA PELOTA A LA DERECHA

                                            laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                            laberinto.celdas[celdaJugador.x+1][celdaJugador.y].tipo='V';
                                            laberinto.celdas[celdaJugador.x][celdaJugador.y].indexSprite=laberinto.derecha;
                                            traeJugador=true;
                                        }
                                    }else{
                                        //SI EL JUGADOR ESTA CON PELOTA Y ENCUENTRA UN OBSTACULO
                                        if(laberinto.lienzoPadre.player == 2){
                                            acomodarPelota(celdaJugador.x, celdaJugador.y);
                                        }

                                        }
                            }else{
                                //SI EL ADVERSARIO ESTA AL FRENTE Y ESTOY CON PELOTA
                                if(laberinto.lienzoPadre.player == 2){
                                   acomodarPelota(celdaJugador.x, celdaJugador.y); 
                                }

                            } 
                                    
                        }else{
                            if((laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y] == laberinto.celdas[32][5]) ||
                                                         (laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y] == laberinto.celdas[32][6]) ||
                                                         (laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y] == laberinto.celdas[32][7]) ||
                                                         (laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y] == laberinto.celdas[32][8]) ||
                                                         (laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y] == laberinto.celdas[32][9]) ||
                                                         (laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y] == laberinto.celdas[32][10] ||
                                                         (laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y] == laberinto.celdas[32][11]) ) ){
                                System.out.println("gol del Jugador");
                                //laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';  
                                laberinto.celdas[celdaJugador.x+1][celdaJugador.y].tipo='V';   

                                laberinto.gol();
                            }else{
                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='V';
                                laberinto.celdaPelota.x=24;
                                laberinto.celdaPelota.y=8;
                                laberinto.saque();
                            }
                        }      
                        
                    }else{
                        //EL JUGADOR SE MUEVE A LA DERECHA
                        
                        laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='V';
                        celdaJugador.x=celdaJugador.x+1;
                        laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                        laberinto.celdas[celdaJugador.x][celdaJugador.y].indexSprite=laberinto.derecha;
                    }
                }
            }
          /*
            if(celdas[celdaJugador.x][celdaJugador.y] == celdas[celdaPelota.x-1][celdaPelota.y]){
               
                celdaPelota.x=celdaJugador.x+2;
                celdas[celdaPelota.x][celdaPelota.y].tipo='L';
                celdas[celdaPelota.x][celdaPelota.y].indexSpritePelota=rueda;
                agarrado=true;
            }
            
            if(agarrado == true){
                celdas[celdaPelota.x][celdaPelota.y].tipo='V';
                celdaPelota.x=celdaJugador.x+2;
                celdaPelota.y=celdaJugador.y;
                celdas[celdaPelota.x][celdaPelota.y].tipo='L';
                celdas[celdaPelota.x][celdaPelota.y].indexSpritePelota=rueda;
                celdas[celdaPelota.x-1][celdaPelota.y].tipo='V';
                
                up=false;
                down=false;
                right=true;
                left=false;
            }
            
            
            celdas[celdaJugador.x][celdaJugador.y].tipo='V';
            celdaJugador.x=celdaJugador.x+1;
            celdas[celdaJugador.x][celdaJugador.y].tipo='J';
            celdas[celdaJugador.x][celdaJugador.y].indexSprite=derecha;
            
            if(derecha == 15){
                derecha=3;
            }
            
            if(rueda >= 2){
                rueda=0;
            }*/
        }
    }
    
    public void moverCeldaArribaIzquierda(){
        
        if(celdaJugador.x > 0 && celdaJugador.y > 1){
            if(laberinto.celdas[celdaJugador.x-1][celdaJugador.y-1].tipo!='A'){
                if(laberinto.celdas[celdaJugador.x-1][celdaJugador.y-1].tipo!='O'){
                    if(laberinto.celdas[celdaJugador.x-1][celdaJugador.y-1].tipo == 'L'){
                        if(laberinto.celdaPelota.x > 0 && laberinto.celdaPelota.y-2 >= 0 ){
                            if(laberinto.celdas[celdaJugador.x-2][celdaJugador.y-2].tipo != 'A'){
                                if(laberinto.celdas[celdaJugador.x-2][celdaJugador.y-2].tipo != 'O'){
                                    if(laberinto.lienzoPadre.player == 1){
                                        //MUEVE LA PELOTA
                                        laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y-1;
                                        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                        laberinto.celdas[celdaJugador.x-1][celdaJugador.y-1].tipo='V'; 
                                        traeJugador=true;
                                    }else{
                                        if(laberinto.celdas[celdaJugador.x][celdaJugador.y-1].tipo != 'A'){
                                            if(laberinto.celdas[celdaJugador.x][celdaJugador.y-1].tipo != 'O'){
                                                //MUEVE LA PELOTA
                                                laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                                celdaJugador.x=celdaJugador.x-1;
                                                celdaJugador.y=celdaJugador.y-1;
                                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                                laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J'; 
                                                laberinto.celdas[celdaJugador.x+1][celdaJugador.y+1].tipo='V'; 
                                                traeJugador=true; 
                                                    
                                            }else{
                                                laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                                laberinto.celdaPelota.y=laberinto.celdaPelota.y-1;
                                                celdaJugador.x=celdaJugador.x-1;
                                                celdaJugador.y=celdaJugador.y-1;
                                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                                laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J'; 
                                                laberinto.celdas[celdaJugador.x+1][celdaJugador.y+1].tipo='V'; 
                                                traeJugador=true; 
                                            }
                                        }else{
                                            laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                            laberinto.celdaPelota.y=laberinto.celdaPelota.y-1;
                                            celdaJugador.x=celdaJugador.x-1;
                                            celdaJugador.y=celdaJugador.y-1;
                                            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                            laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J'; 
                                            laberinto.celdas[celdaJugador.x+1][celdaJugador.y+1].tipo='V'; 
                                            traeJugador=true; 
                                        }
                                    }
  
                                }else{
                                    //CON PELOTA SE PILLA CON OBSTACULO
                                    laberinto.celdas[celdaJugador.x-1][celdaJugador.y-1].tipo='V'; 
                                    laberinto.celdaPelota.x=laberinto.celdaPelota.x+2;
                                    laberinto.celdaPelota.y=laberinto.celdaPelota.y+2;
                                    laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                }
                            }
                        }else{
                            laberinto.lateral();
                            /*
                            //SI CHOCA AL FINAL DE LA CANCHA
                             laberinto.celdas[celdaJugador.x-1][celdaJugador.y-1].tipo='V'; 
                             laberinto.celdaPelota.x=laberinto.celdaPelota.x+2;
                             laberinto.celdaPelota.y=laberinto.celdaPelota.y+2;
                             laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';*/
                        }
                    }else{
                        laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='V';
                        celdaJugador.x=celdaJugador.x-1;
                        celdaJugador.y=celdaJugador.y-1;
                        laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                        laberinto.celdas[celdaJugador.x][celdaJugador.y].indexSprite=laberinto.izquierda;
                    }
            }
        }
        
       /* if(celdaJugador.x >1 && celdaJugador.y > 1 ){
            if(laberinto.celdas[celdaJugador.x][celdaJugador.y] == laberinto.celdas[laberinto.celdaPelota.x+1][laberinto.celdaPelota.y+1]){
                laberinto.celdaPelota.x=celdaJugador.x;
                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].indexSpritePelota=0;
                agarrado=true;
            }
            
            if(agarrado == true){
                laberinto.celdaPelota.x=celdaJugador.x;
                laberinto.celdaPelota.y=celdaJugador.y-1;
                laberinto.celdas[laberinto.celdaPelota.x+1][laberinto.celdaPelota.y+1].tipo='V';
            }
            
            laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='V';
            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].indexSpritePelota=2;
            celdaJugador.x=celdaJugador.x-1;
            celdaJugador.y=celdaJugador.y-1;
            laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
        }*/
        }
    }
    
    public void moverCeldaArribaDerecha(){
        if(celdaJugador.x < anchuraMundoVirtual -1 && celdaJugador.y > 1 ){
            
            if(laberinto.celdas[celdaJugador.x+1][celdaJugador.y-1].tipo!='A'){
                if(laberinto.celdas[celdaJugador.x+1][celdaJugador.y-1].tipo!='O'){
                    if(laberinto.celdas[celdaJugador.x+1][celdaJugador.y-1].tipo == 'L'){
                        if( celdaJugador.y-3 >= 0 ){
                            if(celdaJugador.x+1 < anchuraMundoVirtual-1){
                                if(laberinto.celdas[celdaJugador.x+2][celdaJugador.y-2].tipo != 'A'){
                                    if(laberinto.celdas[celdaJugador.x+2][celdaJugador.y-2].tipo != 'O'){
                                        if(laberinto.lienzoPadre.player == 1){
                                            //MUEVE LA PELOTA
                                            laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                            laberinto.celdaPelota.y=laberinto.celdaPelota.y-1;
                                            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                            laberinto.celdas[celdaJugador.x+1][celdaJugador.y-1].tipo='V'; 
                                            traeJugador=true;
                                        }else{
                                            if(laberinto.celdas[celdaJugador.x+2][celdaJugador.y-1].tipo != 'A'){
                                                if(laberinto.celdas[celdaJugador.x+2][celdaJugador.y-1].tipo != 'O'){
                                                    laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                                    celdaJugador.x=celdaJugador.x+1;
                                                    celdaJugador.y=celdaJugador.y-1;
                                                    laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                                    laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                                                    laberinto.celdas[celdaJugador.x-1][celdaJugador.y+1].tipo='V'; 
                                                    
                                                        //SI LA PELOTA ENTRA AL ARCO
                                                        if((laberinto.celdas[celdaJugador.x][celdaJugador.y] == laberinto.celdas[31][5]) ||
                                                                             (laberinto.celdas[celdaJugador.x][celdaJugador.y] == laberinto.celdas[31][6]) ||
                                                                             (laberinto.celdas[celdaJugador.x][celdaJugador.y] == laberinto.celdas[31][7]) ||
                                                                             (laberinto.celdas[celdaJugador.x][celdaJugador.y] == laberinto.celdas[31][8]) ||
                                                                             (laberinto.celdas[celdaJugador.x][celdaJugador.y] == laberinto.celdas[31][9]) ||
                                                                             (laberinto.celdas[celdaJugador.x][celdaJugador.y] == laberinto.celdas[31][10] ||
                                                                             (laberinto.celdas[celdaJugador.x][celdaJugador.y] == laberinto.celdas[31][11]) ) ){

                                                                System.out.println("gol del Jugador");
                                                                laberinto.lienzoPadre.jugador.gol_jugador=laberinto.lienzoPadre.jugador.gol_jugador+1;
                                                                //laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';  
                                                                laberinto.celdas[celdaJugador.x+1][celdaJugador.y].tipo='V';   
                                                                laberinto.gol();
                                                            }
                                                }else{
                                                    laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                                    laberinto.celdaPelota.y=laberinto.celdaPelota.y-1;
                                                    celdaJugador.x=celdaJugador.x+1;
                                                    celdaJugador.y=celdaJugador.y-1;
                                                    laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                                    laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                                                    laberinto.celdas[celdaJugador.x-1][celdaJugador.y+1].tipo='V'; 
                                                }
                                            }else{
                                                laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                                laberinto.celdaPelota.y=laberinto.celdaPelota.y-1;
                                                celdaJugador.x=celdaJugador.x+1;
                                                celdaJugador.y=celdaJugador.y-1;
                                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                                laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                                                laberinto.celdas[celdaJugador.x-1][celdaJugador.y+1].tipo='V'; 
                                            }
                                        }

                                    }/*else{
                                        //SE ENCUENTRA UN OBSTACULO
                                        laberinto.celdas[celdaJugador.x+1][celdaJugador.y-1].tipo='V';
                                        laberinto.celdaPelota.x=laberinto.celdaPelota.x-2;
                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y+2;
                                        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                    }*/
                                }else{
                                    laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                    celdaJugador.x=celdaJugador.x+1;
                                    celdaJugador.y=celdaJugador.y-1;
                                    laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                    laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                                    laberinto.celdas[celdaJugador.x-1][celdaJugador.y+1].tipo='V'; 
                                }
                            }else{
                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='V';
                                laberinto.celdaPelota.x=24;
                                laberinto.celdaPelota.y=8;
                                laberinto.saque();
                            }
                        }else{
                            laberinto.lateral();
                            /*
                            if(laberinto.lienzoPadre.player == 1){
                                //FINAL DEL MUNDO
                                laberinto.celdas[celdaJugador.x+1][celdaJugador.y-1].tipo='V'; 
                                laberinto.celdaPelota.x=laberinto.celdaPelota.x-2;
                                laberinto.celdaPelota.y=laberinto.celdaPelota.y+2;
                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                            }else{
                                if(laberinto.celdas[celdaJugador.x+2][celdaJugador.y].tipo != 'A'){
                                    if(laberinto.celdas[celdaJugador.x+2][celdaJugador.y].tipo != 'O'){
                                        laberinto.celdas[celdaJugador.x+1][celdaJugador.y-1].tipo='V'; 
                                        laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                    }else{
                                        laberinto.celdas[celdaJugador.x+1][celdaJugador.y-1].tipo='V'; 
                                        laberinto.celdaPelota.x=laberinto.celdaPelota.x-2;
                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y-2;
                                        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                    }
                                }else{
                                    laberinto.celdas[celdaJugador.x+1][celdaJugador.y-1].tipo='V'; 
                                    laberinto.celdaPelota.x=laberinto.celdaPelota.x-2;
                                    laberinto.celdaPelota.y=laberinto.celdaPelota.y-2;
                                    laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                }
                            }
                            
                          */  
                        }
                    }else{
                        laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='V';
                        celdaJugador.x=celdaJugador.x+1;
                        celdaJugador.y=celdaJugador.y-1;
                        laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                        laberinto.celdas[celdaJugador.x][celdaJugador.y].indexSprite=laberinto.derecha;
                    }
                }
            }
            
            
            
           /* if(laberinto.celdas[celdaJugador.x][celdaJugador.y] == laberinto.celdas[laberinto.celdaPelota.x-1][laberinto.celdaPelota.y+1]){
                laberinto.celdaPelota.x=celdaJugador.x+2;
                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].indexSpritePelota=0;
                agarrado=true;
            }
            
            if(agarrado == true){
                laberinto.celdaPelota.y=celdaJugador.y-1;
                laberinto.celdaPelota.x=celdaJugador.x+2;
                laberinto.celdas[laberinto.celdaPelota.x-1][laberinto.celdaPelota.y+1].tipo='V';
            }
            
            laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='V';
            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].indexSpritePelota=2;
            celdaJugador.x=celdaJugador.x+1;
            celdaJugador.y=celdaJugador.y-1;
            laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';*/
        }
    }
    
    public void moverCeldaAbajoIzquierda(){
        if(celdaJugador.x > 0  && celdaJugador.y < alturaMundoVirtual -2){
            
            if(laberinto.celdas[celdaJugador.x-1][celdaJugador.y+1].tipo!='A'){
                if(laberinto.celdas[celdaJugador.x-1][celdaJugador.y+1].tipo!='O'){
                    if(laberinto.celdas[celdaJugador.x-1][celdaJugador.y+1].tipo == 'L'){
                        if(celdaJugador.x-2 >= 0 && celdaJugador.y+3 < alturaMundoVirtual){
                            if(laberinto.celdas[celdaJugador.x-2][celdaJugador.y+2].tipo != 'A'){
                                if(laberinto.celdas[celdaJugador.x-2][celdaJugador.y+2].tipo != 'O'){
                                    
                                    if(laberinto.lienzoPadre.player == 1){
                                        //MUEVE LA PELOTA
                                        laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                        laberinto.celdas[celdaJugador.x-1][celdaJugador.y+1].tipo='V'; 
                                        traeJugador=true;
                                    }else{
                                        if(laberinto.celdas[celdaJugador.x][celdaJugador.y+1].tipo != 'A'){
                                            if(laberinto.celdas[celdaJugador.x][celdaJugador.y+1].tipo != 'O'){
                                                laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                                celdaJugador.x= celdaJugador.x-1;
                                                celdaJugador.y= celdaJugador.y+1;
                                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                                laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J'; 
                                                laberinto.celdas[celdaJugador.x+1][celdaJugador.y-1].tipo='V'; 
                                                traeJugador=true;
                                            }else{
                                                laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                                laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                                celdaJugador.x= celdaJugador.x-1;
                                                celdaJugador.y= celdaJugador.y+1;
                                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                                laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J'; 
                                                laberinto.celdas[celdaJugador.x+1][celdaJugador.y-1].tipo='V'; 
                                                traeJugador=true;
                                            }
                                        }else{
                                            laberinto.celdaPelota.x=laberinto.celdaPelota.x-1;
                                            laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                            celdaJugador.x= celdaJugador.x-1;
                                            celdaJugador.y= celdaJugador.y+1;
                                            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                            laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J'; 
                                            laberinto.celdas[celdaJugador.x+1][celdaJugador.y-1].tipo='V'; 
                                            traeJugador=true;
                                        }

                                    }
                                }else{
                                    //SE ENCUENTRA UN OBSTACULO
                                    laberinto.celdas[celdaJugador.x-1][celdaJugador.y+1].tipo='V'; 
                                    laberinto.celdaPelota.x=laberinto.celdaPelota.x+2;
                                    laberinto.celdaPelota.y=laberinto.celdaPelota.y-2;
                                    laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                }
                            }
                        }else{
                            laberinto.lateral();
                            /*
                            //FINAL DEL MUNDO
                             laberinto.celdas[celdaJugador.x-1][celdaJugador.y+1].tipo='V'; 
                             laberinto.celdaPelota.x=laberinto.celdaPelota.x+2;
                             laberinto.celdaPelota.y=laberinto.celdaPelota.y-2;
                             laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';*/
                        }
                    }else{
                        laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='V';
                        celdaJugador.x=celdaJugador.x-1;
                        celdaJugador.y=celdaJugador.y+1;
                        laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                        laberinto.celdas[celdaJugador.x][celdaJugador.y].indexSprite=laberinto.izquierda;
                    }
                }
            }
            
            
            
            
           /* if(laberinto.celdas[celdaJugador.x][celdaJugador.y] == laberinto.celdas[laberinto.celdaPelota.x+1][laberinto.celdaPelota.y-1]){
                laberinto.celdaPelota.x=celdaJugador.x;
                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].indexSpritePelota=0;
                agarrado=true;
            }
            
            if(agarrado == true){
                laberinto.celdaPelota.y=celdaJugador.y+1;
                laberinto.celdaPelota.x=celdaJugador.x;
                laberinto.celdas[laberinto.celdaPelota.x+1][laberinto.celdaPelota.y-1].tipo='V';
            }
            
            laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='V';
            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].indexSpritePelota=0;
            celdaJugador.x=celdaJugador.x-1;
            celdaJugador.y=celdaJugador.y+1;
            laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';*/
        }
    }
    
    public void  moverCeldaAbajoDerecha(){
        if(celdaJugador.x < anchuraMundoVirtual -1 && celdaJugador.y < alturaMundoVirtual -2 ){
            
            if(laberinto.celdas[celdaJugador.x+1][celdaJugador.y+1].tipo!='A'){
                if(laberinto.celdas[celdaJugador.x+1][celdaJugador.y+1].tipo!='O'){
                    if(laberinto.celdas[celdaJugador.x+1][celdaJugador.y+1].tipo == 'L'){
                        if(celdaJugador.y+3 < alturaMundoVirtual){
                            if(celdaJugador.x+1 < anchuraMundoVirtual-1 ){
                                if(laberinto.celdas[celdaJugador.x+2][celdaJugador.y+2].tipo != 'A'){
                                    if(laberinto.celdas[celdaJugador.x+2][celdaJugador.y+2].tipo != 'O'){
                                        if(laberinto.lienzoPadre.player == 1){
                                            //MUEVE LA PELOTA
                                            laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                            laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                            laberinto.celdas[celdaJugador.x+1][celdaJugador.y+1].tipo='V'; 
                                            traeJugador=true;
                                        }else{
                                            if(laberinto.celdas[celdaJugador.x+2][celdaJugador.y+1].tipo != 'A'){
                                                if(laberinto.celdas[celdaJugador.x+2][celdaJugador.y+1].tipo != 'O'){
                                                    //MUEVE LA PELOTA
                                                    laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                                    celdaJugador.x=celdaJugador.x+1;
                                                    celdaJugador.y=celdaJugador.y+1;
                                                    laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                                                    laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                                    laberinto.celdas[celdaJugador.x-1][celdaJugador.y-1].tipo='V'; 
                                                    
                                                    //SI LA PELOTA ENTRA AL ARCO
                                                    if((laberinto.celdas[celdaJugador.x][celdaJugador.y] == laberinto.celdas[31][5]) ||
                                                                         (laberinto.celdas[celdaJugador.x][celdaJugador.y] == laberinto.celdas[31][6]) ||
                                                                         (laberinto.celdas[celdaJugador.x][celdaJugador.y] == laberinto.celdas[31][7]) ||
                                                                         (laberinto.celdas[celdaJugador.x][celdaJugador.y] == laberinto.celdas[31][8]) ||
                                                                         (laberinto.celdas[celdaJugador.x][celdaJugador.y] == laberinto.celdas[31][9]) ||
                                                                         (laberinto.celdas[celdaJugador.x][celdaJugador.y] == laberinto.celdas[31][10] ||
                                                                         (laberinto.celdas[celdaJugador.x][celdaJugador.y] == laberinto.celdas[31][11]) ) ){

                                                            System.out.println("gol del Jugador");
                                                            laberinto.lienzoPadre.jugador.gol_jugador=laberinto.lienzoPadre.jugador.gol_jugador+1;
                                                            //laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                                            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';  
                                                            laberinto.celdas[celdaJugador.x+1][celdaJugador.y].tipo='V';   
                                                            laberinto.gol();
                                                        }
                                                }else{
                                                    laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                                    laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                                    celdaJugador.x=celdaJugador.x+1;
                                                    celdaJugador.y=celdaJugador.y+1;
                                                    laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                                                    laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                                    laberinto.celdas[celdaJugador.x-1][celdaJugador.y-1].tipo='V'; 
                                                }
                                            }else{
                                                laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                                laberinto.celdaPelota.y=laberinto.celdaPelota.y+1;
                                                celdaJugador.x=celdaJugador.x+1;
                                                celdaJugador.y=celdaJugador.y+1;
                                                laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                                laberinto.celdas[celdaJugador.x-1][celdaJugador.y-1].tipo='V'; 
                                            }
                                        }
                                            
                                        
                                    }else{
                                        //SE ENCUENTRA CON UN OBSTACULO
                                        laberinto.celdas[celdaJugador.x+1][celdaJugador.y+1].tipo='V'; 
                                        laberinto.celdaPelota.x=laberinto.celdaPelota.x-2;
                                        laberinto.celdaPelota.y=laberinto.celdaPelota.y-2;
                                        laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                    }
                                }else{
                                    //MUEVE LA PELOTA
                                    laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                    celdaJugador.x=celdaJugador.x+1;
                                    celdaJugador.y=celdaJugador.y+1;
                                    laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                    laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                                    laberinto.celdas[celdaJugador.x-1][celdaJugador.y-1].tipo='V'; 
                                }
                            }else{
                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='V';
                                laberinto.celdaPelota.x=24;
                                laberinto.celdaPelota.y=8;
                                laberinto.saque();
                            }
                        }else{
                            laberinto.lateral();
                            /*
                            if(laberinto.celdas[celdaJugador.x+2][celdaJugador.y].tipo != 'A'){
                                if(laberinto.celdas[celdaJugador.x-2][celdaJugador.y].tipo != 'O'){
                                    laberinto.celdas[celdaJugador.x+1][celdaJugador.y+1].tipo='V'; 
                                    laberinto.celdaPelota.x=laberinto.celdaPelota.x+1;
                                    laberinto.celdaPelota.y=laberinto.celdaPelota.y-1;
                                    laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                }else{
                                    laberinto.celdas[celdaJugador.x+1][celdaJugador.y+1].tipo='V'; 
                                    laberinto.celdaPelota.x=laberinto.celdaPelota.x-2;
                                    laberinto.celdaPelota.y=laberinto.celdaPelota.y-2;
                                    laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                                }
                            }else{
                                laberinto.celdas[celdaJugador.x+1][celdaJugador.y+1].tipo='V'; 
                                laberinto.celdaPelota.x=laberinto.celdaPelota.x-2;
                                laberinto.celdaPelota.y=laberinto.celdaPelota.y-2;
                                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].tipo='L';
                            }*/
                        }
                    }else{
                        laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='V';
                        celdaJugador.x=celdaJugador.x+1;
                        celdaJugador.y=celdaJugador.y+1;
                        laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                        laberinto.celdas[celdaJugador.x][celdaJugador.y].indexSprite=laberinto.derecha;
                    }
                }
            }
            
            
            
            
           /* if(laberinto.celdas[celdaJugador.x][celdaJugador.y] == laberinto.celdas[laberinto.celdaPelota.x-1][laberinto.celdaPelota.y-1]){
                laberinto.celdaPelota.x=celdaJugador.x+2;
                laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].indexSpritePelota=0;
                agarrado=true;
            }
            
            if(agarrado == true){
                laberinto.celdaPelota.y=celdaJugador.y+1;
                laberinto.celdaPelota.x=celdaJugador.x+2;
                laberinto.celdas[laberinto.celdaPelota.x-1][laberinto.celdaPelota.y-1].tipo='V';
            }
            
            laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='V';
            laberinto.celdas[laberinto.celdaPelota.x][laberinto.celdaPelota.y].indexSpritePelota=0;
            celdaJugador.x=celdaJugador.x+1;
            celdaJugador.y=celdaJugador.y+1;
            laberinto.celdas[celdaJugador.x][celdaJugador.y].tipo='J';*/
        }
    }

    @Override
    public synchronized void run() {
        Estado subInicial, subObjetivo;
        boolean resultado;
        int i=0;
      if(! parar){
          
          buscaPelota.colaEstados.clear();
          buscaPelota.historial.clear();
          buscaPelota.pasos.clear();
          
          llevaPelota.colaEstados.clear();
          llevaPelota.historial.clear();
          llevaPelota.pasos.clear();
          
          //do{
              subInicial= new Estado(celdaJugador.x, celdaJugador.y,'N', null);
              //subObjetivo = destinos.get(0);
              subObjetivo= new Estado(laberinto.celdaPelota.x, 
                        laberinto.celdaPelota.y, 'N', null);
              
              System.out.println("subObjetivo Jugador: " + subObjetivo);
             
              if(traeJugador == true){
                  
                  if(celdaJugador.y < 5){
                         gol_abajo=true;
                         subObjetivo = new Estado(32, 5,'N', null);   
                     }
                  switch(celdaJugador.y){
                      case 5:
                            subObjetivo = new Estado(32, 5,'N', null); 
                            break;
                        case 6:
                            subObjetivo = new Estado(32, 6,'N', null); 
                            break;
                        case 7:
                            subObjetivo = new Estado(32, 7,'N', null); 
                            break;
                        case 8:
                            subObjetivo = new Estado(32, 8,'N', null); 
                            break;
                        case 9:
                            subObjetivo = new Estado(32, 9,'N', null); 
                            break;
                        case 10:
                            subObjetivo = new Estado(32, 10,'N', null); 
                            break;
                        case 11:
                            subObjetivo = new Estado(32, 11,'N', null); 
                            break;
                  }
                  
                  if(celdaJugador.y > 11 ){
                        gol_arriba=true;
                        subObjetivo = new Estado(32, 11,'N', null); 
                     } 

                  resultado=buscaPelota.buscar(subInicial, subObjetivo);
                  traeJugador=false;
              }else{
                  resultado=llevaPelota.buscar(subInicial, subObjetivo);
              }
                
            /*  if(subInicial.equals(subObjetivo)){
                  destinos.remove(subObjetivo);
                    }

              if(destinos.isEmpty()){
                  System.out.println("Se acabo a donde ir");
                  parar=true;
                  this.cancel();
              } */
              
             /* if(resultado == false){
                colaEstados.clear();
                historial.clear();
                pasos.clear();
                destinos.remove(subObjetivo);
              }*/
             
         // }while(!resultado && !destinos.isEmpty());
          
          if(buscaPelota.pasos.size() > 1 ){
              switch(buscaPelota.pasos.get(1)){
                  case 'D': this.moverCeldaAbajo();break;
                  case 'U': this.moverCeldaArriba();break;
                  case 'R': this.moverCeldaDerecha();break;
                  case 'L': this.moverCeldaIzquierda();break;
                  case 'X': this.moverCeldaArribaDerecha();break;
                  case 'Y': this.moverCeldaArribaIzquierda();break;
                  case 'W': this.moverCeldaAbajoDerecha(); break;
                  case 'Z': this.moverCeldaAbajoIzquierda();break;
              }
              
              laberinto.lienzoPadre.repaint();
          }
         
          if(llevaPelota.pasos.size() > 1 ){
              switch(llevaPelota.pasos.get(1)){
                  case 'D': laberinto.lienzoPadre.jugador.moverCeldaAbajo();break;
                  case 'U': laberinto.lienzoPadre.jugador.moverCeldaArriba();break;
                  case 'R': laberinto.lienzoPadre.jugador.moverCeldaDerecha();break;
                  case 'L': laberinto.lienzoPadre.jugador.moverCeldaIzquierda();break;
                  case 'X':
                      if(laberinto.lienzoPadre.levelJ == 2 ){
                        laberinto.lienzoPadre.jugador.moverCeldaArribaDerecha();  
                      }
                      break;
                  case 'Y': 
                      if(laberinto.lienzoPadre.levelJ == 2 ){
                        laberinto.lienzoPadre.jugador.moverCeldaArribaIzquierda();
                      }
                      break;
                  case 'W': 
                      if(laberinto.lienzoPadre.levelJ == 2 ){
                        laberinto.lienzoPadre.jugador.moverCeldaAbajoDerecha();
                      }
                      break;
                  case 'Z': 
                      if(laberinto.lienzoPadre.levelJ == 2 ){
                        laberinto.lienzoPadre.jugador.moverCeldaAbajoIzquierda();
                      }
                      break;
              }
              
              laberinto.lienzoPadre.repaint();
          }
          
      }
    }
     
 
    
}
