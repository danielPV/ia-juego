/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego_futbol_ia;

/* Paquetes que se utilizaran*/ 

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import javax.swing.JComponent;

/**
 *
 * @author DANIEL
 */

/* Clase Celda que era de JComponent e implementa Constantes */
public class Celda extends JComponent implements Constantes{
     
    //Posicion x e y de la Celda
    public int x;
    public int y;
    
    //atributos para imagenes
    public char tipo;
    public BufferedImage jugador,obstaculo,camino, adversario, patear, pelota, arco;

    //PARA ANIMACIONES JUGADOR
    //para moverse en el sprite
    public int indexSprite;
    public int indexSpritePelota;
    public int indexSpritePatear;
    //sprite almacenara las imagenes y imagenSprites almacena la imagen de los sprites
    public BufferedImage sprites[],imagenSprites, spritesPelota[], imagenSpritesPelota;
    public BufferedImage spritesPatear[], imagenSpritePatear;
    
    //variable que indica que una celda fue seleccionada
    //puede cambiar durante la ejecucion
    
    //Celda que se movera
    public Celda celdaMovimiento; 
    
    //Constructor
    public Celda(int x, int y, char tipo){
        this.x=x;
        this.y=y; 
        this.tipo=tipo;
        //Indice que corresponde a una imagen de frente
        indexSprite=3;
        indexSpritePelota=0;
       try {
            jugador = ImageIO.read(new File("Imagenes/jugador.png"));
            obstaculo = ImageIO.read(new File("Imagenes/obstaculo.png"));
            arco = ImageIO.read(new File("Imagenes/arco.png"));
            adversario = ImageIO.read(new File("Imagenes/adversario.png"));
            patear = ImageIO.read(new File("Imagenes/patear.png"));
            pelota = ImageIO.read(new File("Imagenes/pelota.png"));
            
            //GESTOR DE SPRITES
            //cargo el grupo de Sprites
            imagenSprites=ImageIO.read(new File("Imagenes/megaman.png"));
            imagenSpritesPelota=ImageIO.read(new File("Imagenes/pelota.png"));
            imagenSpritePatear=ImageIO.read(new File("Imagenes/patear.png"));
            
            //creo un array de 4 x 4 para jugador
            sprites = new BufferedImage[4 * 4];
            //creo un array de 2 x 4 para pelota
            spritesPelota = new BufferedImage[4 * 2];
            //creo un array de 2 x 4 para patear
            spritesPatear = new BufferedImage[4 * 2];
            
            //lo recorro separando las imagenes
            for(int i=0;i<4;i++){
                for(int j=0;j<4;j++){
                    sprites[(i * 4) + j] = 
                    imagenSprites.getSubimage(i*anchuraCelda,
                                              j*alturaCelda,
                                              anchuraCelda,alturaCelda);
                }
            }
            
            for(int a=0;a<2;a++){
                for(int b=0;b<4;b++){
                    spritesPelota[(a * 4) + b] = 
                    imagenSpritesPelota.getSubimage(b*anchuraCelda,
                                                a*alturaCelda, 
                                                anchuraCelda, alturaCelda);
                }
            }
            
            for(int c=0; c<2;c++){
                for(int d=0;d<4;d++){
                    spritesPatear[(c*4)+d]=
                    imagenSpritePatear.getSubimage(d*anchuraCelda,
                                                c*alturaCelda,
                                                anchuraCelda, alturaCelda);
                }
            }
            
        } catch (IOException e) {
            System.out.println(e.toString());
        }

    }
    
    //PARA DOBLE BUFFERING
    
    @Override
    public void update(Graphics g) {
        switch(tipo) {
           // case 'J': g.drawImage(jugador,x,y,anchuraCelda,alturaCelda, null,this); break;
            case 'J': g.drawImage(sprites[indexSprite], x, y,anchuraCelda, alturaCelda, null);break;
            case 'O': g.drawImage(obstaculo,x,y, this); break;
            case 'L': g.drawImage(spritesPelota[indexSpritePelota], x, y, null); break;
           /* case 'V': g.setColor(COLORFONDO); 
            g.fillRect(x, y,anchuraCelda,alturaCelda);
            break; */
            case 'V': g.drawImage(camino,x,y, this); break;
            case 'C': g.drawImage(arco,x,y, this); break;
            case 'A': g.drawImage(adversario,x,y,anchuraCelda,alturaCelda, null); break;
            case 'P': g.drawImage(spritesPatear[indexSpritePatear], x, y,anchuraCelda, alturaCelda, null); break;
        }
    }

    
    //Metodo para dibujar celdas, hace uso de DrawRect
    @Override
    /*Dibuja un rectangulo en la posicion x e y, de ancho Ancho y largo Largo. */
    public void paintComponent(Graphics g){
       update(g);
    }
    
    //Si el click esta sobre la celda
    
   public boolean celdaSeleccionada(int xp, int yp){
       Rectangle r= new Rectangle(x,y,anchuraCelda, alturaCelda);
       return r.contains(new Point(xp,yp));
   } 
   

    
    
}
