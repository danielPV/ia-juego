/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego_futbol_ia;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import javax.swing.JComponent;
//para hacer los numeros ramdons
import java.util.Random;
import java.util.Timer;
import java.util.concurrent.TimeUnit;
import javax.swing.JOptionPane;


/**
 *
 * @author DANIEL
 */
public class Laberinto extends JComponent implements Constantes{
    //dimensiones del laberinto
    public int anchuraLaberinto, alturaLaberinto;
    //las casillas n x m
    public Celda[][] celdas;
    //Celda Jugador
    public Celda celdaJugador;
    //Celda ADVERSARIO
    public Celda celdaAdversario;
    //celda PELOTA
    public Celda celdaPelota;
    //celda OBSTACULOS
    public Celda celdaObstaculos;
    //celda Arcos
    public Celda celdaArcosJ, celdaArcosA;
    
    //PARA ANIMACION ADVERSARIO
    public Lienzo lienzoPadre;
    
    //Cuando se agarra la pelota
    boolean agarrado=false;
    
    //Para patear
    boolean up=false;
    boolean down=false;
    boolean right=false;
    boolean left=false;
    
    //Para animacion personaje
    int arriba=0;
    int abajo=2;
    int derecha=3;
    int izquierda=1;
    //animacion pelota
    int rueda=0;
    
    public Timer lanzadorTareas2;
    
    
    
    
    //PARA ANIMACION ADVERSARIO
    public Laberinto(Lienzo lienzoPadre){
        
        this.lienzoPadre=lienzoPadre;
        celdas = new Celda[anchuraMundoVirtual][alturaMundoVirtual];
        //inicializar el array de celdas
        for(int i=0; i<anchuraMundoVirtual; i++)
            for(int j=0; j<alturaMundoVirtual;j++)
                celdas[i][j]=new Celda(i+(i*anchuraCelda), j+(j*alturaCelda), 'V');
        
        celdaJugador = new Celda(5,8,'J');

    //celdas que se moveran
    //posicion inicial
    //celdaJugador=new Celda(3,7, 'J');
    //celdaAdversario=new Celda(22,7,'A');
    celdaPelota=new Celda(16,8,'L');
    

    //obstaculo siempre ahi
    /*celdas[14][8].tipo='O';
    celdas[17][10].tipo='O';
    celdas[4][1].tipo='O';*/
    
    
    //ARCOS
    for(int i=5;i<12;i++){
        celdaArcosJ=new Celda(0,i,'C');
        celdas[celdaArcosJ.x][celdaArcosJ.y].tipo='C';
    }
    
    for(int i=5;i<12;i++){
        celdaArcosA=new Celda(32,i,'C');
        celdas[celdaArcosA.x][celdaArcosA.y].tipo='C';
    }

    //movimiento jugador
    //celdas[celdaJugador.x][celdaJugador.y].tipo='J';
    
    //movimiento adversario
    //celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
    
    //movimiento pelota
    celdas[celdaPelota.x][celdaPelota.y].tipo='L';

        
    //ancho y largo del laberinto
    this.anchuraLaberinto=anchuraMundoVirtual*anchuraCelda;
    this.alturaLaberinto=alturaMundoVirtual*alturaCelda;
    this.setSize(anchuraLaberinto, alturaLaberinto);
    }
    
    //PARA DOBLE BUFFERING
    
    @Override
    public void update(Graphics g) {
        for(int i=0; i < anchuraMundoVirtual ; i++)
            for ( int j=0 ; j < alturaMundoVirtual; j++)
                celdas[i][j].update(g);
    }

    
    @Override
    public void paintComponent(Graphics g){
        update(g);
    }
    
    public int calculaObstaculosA(int x, int y){
        
        int num=0;

         while(celdas[x][y].tipo != 'O' && x>1 ){
             x=x-1;
             num++;
         }

        return num;
        
    }
    
    public int calculaObstaculosJ(int x, int y){
        
        int num=0;

         while(celdas[x][y].tipo != 'O' && x<32 ){
             x=x+1;
             num++;
         }

        return num;
        
    }
    
    public void saque(){

        System.out.println("Saque de arco");
 
        celdas[celdaPelota.x][celdaPelota.y].tipo='L'; 
    }
    
    public void lateral(){
        celdas[celdaPelota.x][celdaPelota.y].tipo='V'; 
        
        Random  pos_x = new Random();  
        Random  pos_y = new Random();
        
        celdaPelota.x=(int)(pos_x.nextDouble() * (26 - 2) + 2 );
        celdaPelota.y= (int)(pos_y.nextDouble() * (12 - 1) + 1 );
        
        System.out.println("Lateral");
 
        celdas[celdaPelota.x][celdaPelota.y].tipo='L'; 
    }
    
    public void gol(){
            
           //celdas[lienzoPadre.jugador.celdaJugador.x+1][lienzoPadre.jugador.celdaJugador.y].tipo='V';
           //celdas[celdaPelota.x][celdaPelota.y].tipo='L';
           //lienzoPadre.lanzadorTareas.cancel();
           lienzoPadre.pause();
           //celdaPelota.x=celdaPelota.x+1;
           celdas[celdaPelota.x][celdaPelota.y].tipo='L'; 
           
           
           
           JOptionPane.showMessageDialog(null,"GOOOOOOLLLLLLLLL!!!!");
           //JOptionPane.showMessageDialog(null,"Megaman: "+lienzoPadre.jugador.gol_jugador+ "\nZero: "+lienzoPadre.adversario.gol_adversario );
          //INICIALIZAMOS
          
          //BORRAMOS LA ULTIMA POSICION DEL ADVERSARIO Y JUGADOR
          celdas[lienzoPadre.adversario.celdaAdversario.x][lienzoPadre.adversario.celdaAdversario.y].tipo='V';
          celdas[lienzoPadre.jugador.celdaJugador.x][lienzoPadre.jugador.celdaJugador.y].tipo='V';

          lienzoPadre.adversario.celdaAdversario.x=27;
          lienzoPadre.adversario.celdaAdversario.y=8;
          
          lienzoPadre.jugador.celdaJugador.x=5;
          lienzoPadre.jugador.celdaJugador.y=8;

          celdaPelota.x=16;
          celdaPelota.y=8;
   
          celdas[celdaPelota.x][celdaPelota.y].tipo='L';
          celdas[celdaJugador.x][celdaJugador.y].tipo='J';
          celdas[lienzoPadre.adversario.celdaAdversario.x][lienzoPadre.adversario.celdaAdversario.y].tipo='A';
          //lienzoPadre.adversario.moverAdversarioAbajo();
          
           //ARCOS
            for(int i=5;i<12;i++){
                celdaArcosJ=new Celda(0,i,'C');
                celdas[celdaArcosJ.x][celdaArcosJ.y].tipo='C';
            }

            for(int i=5;i<12;i++){
                celdaArcosA=new Celda(32,i,'C');
                celdas[celdaArcosA.x][celdaArcosA.y].tipo='C';
            }
            
            //Se da vida al adversario nuevamente
          // lanzadorTareas2=new Timer();
           //lanzadorTareas2.scheduleAtFixedRate(lienzoPadre.adversario.buscaPelota,0,500);
           if(lienzoPadre.exec.isShutdown()){
               lienzoPadre.continuar();
           }
           
         // lienzoPadre.start(0, 1000);
    }
    
    
    
    public void moverCelda( KeyEvent evento ) {
            switch( evento.getKeyCode() ) {
                case KeyEvent.VK_NUMPAD8:
                    System.out.println("Mover arriba");
                    lienzoPadre.jugador.moverCeldaArriba();
                    arriba=arriba+4;
                    rueda=rueda+1;
                    if(arriba == 12){
                        arriba=0;
                    }
                    if(rueda >= 2){
                        rueda=0;
                    }
                    break;
                case KeyEvent.VK_NUMPAD5 :
                    System.out.println("Mover abajo");
                    lienzoPadre.jugador.moverCeldaAbajo();
                    abajo=abajo+4;
                    rueda=rueda+1;
                    if(abajo == 14){
                        abajo=2;
                    }
                    if(rueda >= 2){
                        rueda=0;
                    }
                    break;
                case KeyEvent.VK_NUMPAD4:
                    System.out.println("Mover izquierda");
                    lienzoPadre.jugador.moverCeldaIzquierda();
                    izquierda=izquierda+4;
                    rueda=rueda+1;
                    if(izquierda == 13){
                        izquierda=1;
                    }
                    if(rueda >= 2){
                        rueda=0;
                    }
                    break;
                case KeyEvent.VK_NUMPAD6:
                    System.out.println("Mover derecha");
                    lienzoPadre.jugador.moverCeldaDerecha();
                    derecha=derecha+4;
                    rueda=rueda+1;
                    if(derecha == 15){
                        derecha=3;
                    }
                    if(rueda >= 2){
                        rueda=0;
                    }
                    break;
                case KeyEvent.VK_NUMPAD7:
                    System.out.println("Mover arriba Izquierda");
                    lienzoPadre.jugador.moverCeldaArribaIzquierda();
                    izquierda=izquierda+4;
                    if(izquierda == 13){
                        izquierda=1;
                    }
                    break;
                case KeyEvent.VK_NUMPAD9:
                    System.out.println("Mover arriba derecha");
                    lienzoPadre.jugador.moverCeldaArribaDerecha();
                    derecha=derecha+4;
                    if(derecha == 15){
                        derecha=3;
                    }
                    break;
                case KeyEvent.VK_NUMPAD1:
                    System.out.println("Mover abajo izquierda");
                    lienzoPadre.jugador.moverCeldaAbajoIzquierda();
                    izquierda=izquierda+4;
                    if(izquierda == 13){
                        izquierda=1;
                    }
                    break;
                case KeyEvent.VK_NUMPAD3:
                    System.out.println("Mover abajo derecha");
                    lienzoPadre.jugador.moverCeldaAbajoDerecha();
                    derecha=derecha+4;
                    if(derecha == 15){
                        derecha=3;
                    }
                    break;
                /*case KeyEvent.VK_SPACE:
                    System.out.println("Cargar");
                    lienzoPadre.jugador.cargar();
                    break;*/
                                        }
    }
  
    
    
}
