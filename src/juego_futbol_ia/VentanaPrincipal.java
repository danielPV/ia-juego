/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego_futbol_ia;

/* paquetes que utilizaremos:
-la clase JFrame nos proporciona funcionalidad para crear ventanas
-la clase BorderLayout nos proporciona funcionalidad para distribuir los
elemtnos graficos */

import java.awt.BorderLayout;
import javax.swing.JFrame;
import java.io.IOException;
import javax.swing.JSplitPane;

/**
 *
 * @author DANIEL
 */

/* clase VetanaPrincipal hereda de JFrame para obtener funcionalidad
de creacion de ventanas graficas
*/

public class VentanaPrincipal extends JFrame implements Constantes{
    
//Muestra clase se compone de un lienzo de dibujo (hereda de canvas)
    public Lienzo lienzo;
    //Objeto tipo hilo musica
    public HiloMusica publico;
    
    //para poner panel de configuracion
    public PanelConfiguracion panelConfiguracion;
    
    //GUARDAR ANCHO Y ALTO DE LA PANTALLA
    int ancho = java.awt.Toolkit.getDefaultToolkit().getScreenSize().width;
    int alto = java.awt.Toolkit.getDefaultToolkit().getScreenSize().height;
    
    int player, levelA, speed, levelJ;
//Constructor
public VentanaPrincipal(int player, int levelA, int speed, int levelJ){
    this.player=player;
    this.levelA=levelA;
    this.speed=speed;
    this.levelJ=levelJ;
    this.setName("JUEGO");
    lienzo= new Lienzo(player, levelA, speed, levelJ);
    lienzo.setName("MEGA SOCCER");
    /*
    panelSeparador=new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
    panelSeparador.setOneTouchExpandable(true);
    
    panelJuego= new PanelDeJuego();
    
    panelConfiguracion=new PanelConfiguracion(panelJuego);
    
    panelSeparador.setLeftComponent(panelJuego);
    panelSeparador.setRightComponent(panelConfiguracion);
    panelSeparador.setDividerLocation(panelJuego.getWidth()-20);
    panelSeparador.setDividerSize(8);
    
    getContentPane().setLayout(new BorderLayout());
    getContentPane().add(panelSeparador, BorderLayout.CENTER);
    this.setSize(ancho,alto);*/
    
    //musica del publico
    publico= new HiloMusica(RUTA+"/Musica/publico.wav",99);
    //publico.run();
    
    lienzo.setFocusable(true);
    lienzo.requestFocus();
    this.getContentPane().setLayout(new BorderLayout());
    this.getContentPane().add(lienzo);
    this.setSize(ancho, alto);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
}    



}

