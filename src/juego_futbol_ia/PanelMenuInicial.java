/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego_futbol_ia;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


/**
 *
 * @author DANIEL
 */
public class PanelMenuInicial extends JPanel implements Constantes{
    
    public JButton comenzar, salir;
    public String[] tipos_letra;
    public JFrame ventana_principal;
    public HiloMusica inicio;
    
    public PanelMenuInicial(JFrame ventana_principal){
        this.ventana_principal=ventana_principal;
        
        this.ventana_principal.setName("MEGA SOCCER");
        this.setLayout(null);
        this.setBackground(Color.BLUE);
        this.setSize(SCREEN_SIZE.width,SCREEN_SIZE.height);
        
        comenzar=new JButton("P R E S S   S T A R T");
        comenzar.setBounds(1000, 550, 310, 100);
        comenzar.setOpaque(true);
        comenzar.setFont(new Font("Times New Roman", Font.BOLD,20));
        comenzar.setBackground(Color.BLUE);
        comenzar.setForeground(Color.WHITE);
        comenzar.setHorizontalTextPosition(JButton.CENTER);
        comenzar.setVerticalTextPosition(JButton.CENTER);
        comenzar.addActionListener(this::pulsarBotonComenzar);
        
        salir=new JButton("E X I T");
        salir.setBounds(650, 550, 310, 100);
        salir.setOpaque(true);
        salir.setFont(new Font("Times New Roman", Font.BOLD,20));
        salir.setBackground(Color.BLUE);
        salir.setForeground(Color.WHITE);
        salir.setHorizontalTextPosition(JButton.CENTER);
        salir.setVerticalTextPosition(JButton.CENTER);
        salir.addActionListener(this::pulsarBotonSalir);
        
        inicio= new HiloMusica(RUTA+"/Musica/inicio.wav",99);
        //inicio.run();
        
        add(comenzar);
        add(salir);
        
    }
    
    public void pulsarBotonComenzar(ActionEvent e){
       // VentanaPrincipal ventanaPrincipal = new VentanaPrincipal();
       // ventanaPrincipal.setVisible(true);
       
       VentanaMenuConfiguracion panelConfiguracion = new VentanaMenuConfiguracion();
       panelConfiguracion.setVisible(true);
       ventana_principal.setVisible(false);
       ventana_principal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    public void pulsarBotonSalir(ActionEvent e){
        ventana_principal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        System.exit(0);
    }
    
    @Override
    public void paintComponent (Graphics g){
        Dimension d =getSize();
        BufferedImage fondo = null;
        try{
            fondo=ImageIO.read(new File(RUTA_DIRECTORIO+"/Imagenes/fondo.jpg"));
        }catch(IOException ex){
            Logger.getLogger(PanelMenuInicial.class.getName()).log(Level.SEVERE, null, ex);
        }
        g.drawImage(fondo, 0, 0, d.width, d.height, null);
        super.paintComponents(g);
    }
    
}
